# ########################################
# main function
# ########################################
solar_postprocess = function(power_df,nodalId,FxDate,FxBlockID,train_forecast,weather_forecast_source,
                             dayahead_intraday,orgId,pssId,n_days,cumulative_fx){
  
  if(!is.data.frame(power_df) | identical(character(0),power_df)){
    
    path = paste(getwd(),"new_files","SOLAR","LOGS","DAILY_FORECAST_ERROR",year(FxDate),
                 month(FxDate),day(FxDate),orgId,pssId,nodalId,sep = "/")
    
    if (!dir.exists(path)) {
      Sys.umask("002")
      dir.create(path, recursive = TRUE)
    }
    report =paste(path,"/ERROR_LOG","_",year(FxDate),"_",month(FxDate),
                  "_",day(FxDate),"_",orgId,"_",pssId,"_",nodalId,
                  "_","SIMPLE_SOLAR","_",weather_forecast_source,".csv",sep = "")
    
    if(isAutoFx==1){
      write.csv(power_df,report,row.names = FALSE,quote = FALSE)
    }
    source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
    power_df <- simplesolar_postprocessed_fx(nodalId, FxDate,FxBlockID,weather_forecast_source=1, 
                                             dayahead_intraday,orgId, pssId,cumulative_fx,input=data.frame(), avc_df=data.frame())
    
  }else
    if(is.data.frame(power_df) & nrow(power_df)<3){
      path = paste(getwd(),"new_files","SOLAR","LOGS","DAILY_FORECAST_ERROR",
                   year(FxDate),month(FxDate),day(FxDate),orgId,pssId,nodalId,sep = "/")
      if (!dir.exists(path)) {
        Sys.umask("002")
        dir.create(path, recursive = TRUE)
      }
      report =paste(path,"/ERROR_LOG","_",year(FxDate),"_",month(FxDate),"_",day(FxDate),
                    "_",orgId,"_",pssId,"_",nodalId,"_",model_type_a,"_",weather_forecast_source,".csv",sep = "")
      if(isAutoFx==1){
        write.csv(power_df,report,row.names = FALSE,quote = FALSE) 
      }
      #message("Error in generation. Using default combination")
      source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
      power_df <- simplesolar_postprocessed_fx(nodalId, FxDate,FxBlockID,weather_forecast_source=1, 
                                               dayahead_intraday,orgId, pssId,cumulative_fx,input=data.frame(), 
                                               avc_df=data.frame())
    }
  avc_df = get_available_cap( nodalId, projType, orgId, pssId, StartDate,EndDate)
  SF_capacity = getParkCapacityByNodalID( nodalId, projType,  orgId, pssId)
  if(length(avc_df) ==0) #message ("No AVC")
    avc_df = avc_df %>% dplyr::filter(BLOCK %in% power_df$bid)    
  power_df = power_df %>% transform(fx= fx * avc_df$AVL_QTM/SF_capacity)
  
  #Keep max forecasted power as SF_capacity
  power_df$fx= ifelse(power_df$fx > SF_capacity*1000, SF_capacity*1000, 
                      power_df$fx)
  #message("before DSM_RATE")
  DSM_rate = get_DSM_Rate(nodalId,orgId, pssId)
  DSM_rate = DSM_rate %>% filter(DSM_RATE == 0)
  if(orgId == 70){
    DSM_rate = DSM_rate[1,"UPPER_BAND"]
  }else{
    DSM_rate = DSM_rate$UPPER_BAND
  }
  #message("DSM_rate: ",DSM_rate)
  lat_long = get_lat_long( nodalId, orgId, pssId,projType)
  
  if(nrow(lat_long)!= 0){
    lat = lat_long[1,1]
    long = lat_long[1,2]
    source(paste(getwd(),"SOLAR/COMMON_FILES/SUNRISE_SUNSET.R",sep = "/"))
    sunrise_sunset = sunrise_set(FxDate, lat, long)
    #if (length(sunrise_sunset)!= 0 ){
    # print (sunrise_sunset)
    sunrise = floor(getBlockIdFromDateTime(hms(sunrise_sunset[1])) ) #floor is used, because first non-zero is the ceiling
    # print (sunrise)
    sunset = ceiling(getBlockIdFromDateTime(hms(sunrise_sunset[2])) )
    # print (sunset)
    
    if(orgId == 70){
      # Since likupang and Gorontolo share the same time zone-WITA (Central Indonesian Time),
      #WITA is 8 hours ahead Greenwich Mean Time (GMT) and is used in Asia. So we add 10 time
      #blocks to make it to IST
      
      sunrise = sunrise + 10
      sunset = sunset + 10
      
      #message("10 time blocks added to sunrise and sunset for Tetratech plants")
      # print (sunrise)
      # print (sunset)
    }
    
    if( cumulative_fx != 1 ){
      power_df[power_df$bid %in% 1:sunrise,"fx"] = 0
      power_df[power_df$bid %in% sunset:num_blocks,"fx"] = 0
    }
  }
  
  power_df$fx = ifelse(power_df$fx < 0, 0, power_df$fx)
  #Set the lower bound of Fx power using the first dsm band from 0900 hrs to 1500 hrs.
  if(nrow(DSM_rate)>2 & !orgId %in% c(47,70,5)){
    avc_df = avc_df %>% dplyr::filter(BLOCK %in% power_df$bid)
    power_df$fx = ifelse( power_df$bid %in% c(37:60) & 
                            power_df$fx < avc_df$AVL_QTM*DSM_rate*0.01*1000, 
                          avc_df$AVL_QTM*DSM_rate*0.01*1000,power_df$fx)
    #message("Lower bound of Power with first DSM band applied")
  }
  power_df = power_df %>% dplyr:: arrange(ddt,bid)
  # ---------------------------------------------
  # Aggregate - Output
  if (nodalId == -1){
    power_df$nodalId = NULL 
    power_df = cbind(Pss_Id = pssId, power_df)
    power_df = cbind(Org_Id = orgId, power_df)
  }
  
  ####################################################
  
  return (power_df)
}
#################################################################################################

## Default fx for error in fx generation 
get_default_fx = function(power_df,nodalId, FxDate, FxBlockID,
                          train_forecast,
                          weather_forecast_source,
                          dayahead_intraday, orgId, pssId,
                          n_days,cumulative_FX){
  # -------------------------------------
  #-Check if power_df is dataframe or not
  # -------------------------------------
  
  if(!is.data.frame(power_df)|identical(character(0),power_df)){
    
    path = paste(getwd(),"new_files","SOLAR","LOGS","DAILY_FORECAST_ERROR",year(FxDate),month(FxDate),
                 day(FxDate),orgId,pssId,nodalId,sep = "/")
    
    if (!dir.exists(path)) {
      Sys.umask("002")
      dir.create(path, recursive = TRUE)
    }
    report =paste(path,"/ERROR_LOG","_",year(FxDate),"_",month(FxDate),
                  "_",day(FxDate),"_",orgId,"_",pssId,"_",nodalId,
                  "_","SIMPLE_SOLAR","_",weather_forecast_source,".csv",sep = "")
    
    if(isAutoFx==1){
      write.csv(power_df,report,row.names = FALSE,quote = FALSE) 
    }
    
    source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
    power_df <- simplesolar_postprocessed_fx(nodalId, FxDate,FxBlockID,
                                             weather_forecast_source=1, 
                                             dayahead_intraday,orgId, pssId,
                                             cumulative_fx,input=data.frame(), avc_df=data.frame())
  }else
    if(is.data.frame(power_df) & nrow(power_df)<3){
      path = paste("new_files","SOLAR","DAILY_FORECAST_ERROR",
                   year(FxDate),month(FxDate),day(FxDate),orgId,pssId,nodalId,sep = "/")
      
      if (!dir.exists(path)) {
        Sys.umask("002")
        dir.create(path, recursive = TRUE)
      }
      report =paste(path,"/ERROR_LOG","_",year(FxDate),"_",month(FxDate),"_",day(FxDate),
                    "_",orgId,"_",pssId,"_",nodalId,
                    "_","SIMPLE_SOLAR","_",weather_forecast_source,".csv",sep = "")
      if(isAutoFx==1){
        write.csv(power_df,report,row.names = FALSE,quote = FALSE) 
      }
      
      #message("Error in generation. Using default combination")
      source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
      power_df <- simplesolar_postprocessed_fx(nodalId, FxDate,FxBlockID,
                                               weather_forecast_source=1, 
                                               dayahead_intraday,orgId, pssId,
                                               cumulative_fx,input=data.frame(), avc_df=data.frame())
      
    }
  
}

#####################################
# Outputs the parameters used in formula
#####################################

all_getParameters = function(nodalId, FxDate, model_type_a, orgId, pssId) {
  if (model_type_a %in% c(3,7) ){ #LR
    source("C:/R_Services/RE_WIND_SOLAR/SOLAR1/LR_dayahead_intraday.R", local=TRUE)
    par_df =  getParameters(nodalId, FxDate, model_type=2, orgId, pssId) 
  } else if (model_type_a == 4){# Theoretical 1
    source("C:/R_Services/RE_WIND_SOLAR/SOLAR1/theory_solar_forecast_fullday.R", local=TRUE)
    par_df =  getParameters(nodalId, FxDate, model_type=1, orgId, pssId) 
  } else if (model_type_a == 5 | model_type_a == 6){# Theoretical 1
    source("C:/R_Services/RE_WIND_SOLAR/SOLAR1/theory_solar_forecast_fullday.R", local=TRUE)
    par_df =  getParameters(nodalId, FxDate, model_type=2, orgId, pssId) 
  } 
}

############ Function to validate input parameters ################
val_par_fun = function(FxDate, weather_forecast_source,
                       nodalId,dayahead_intraday, orgId, pssId,FxBlockID,cumulative_fx){
  
  if(!dayahead_intraday %in% c(0,1,2) | FxBlockID > 96 )return(0)
  
  #Check if required.
  if(FxDate > (Sys.Date() + 7))return(0)
  
  if(nodalId %in% farm_df$FARM_ID){
    temp = farm_df[which(farm_df$FARM_ID == nodalId),]
    if(!orgId %in% temp$ORG_ID | !pssId %in% temp$PSS_ID) return(2)
  } else {
    return(2)
  }
  
  farm_df = get_fxdate_schedules( FxDate)
  if(is.null( farm_df)){return(3)}
  # else
  # {
  #   farm_df = farm_df%>%filter(FX_TYPE == dayahead_intraday)
  # }
  
  res = get_all_weather_sources( nodalId, orgId, pssId)
  if(!weather_forecast_source %in% res$WFxId) return(4)
  
  SF_capacity = getParkCapacityByNodalID( nodalId, projType,  orgId, pssId)
  if(is.null( SF_capacity)){return(5)}
  else{ input = get_all_weather_data_solar(nodalId, FxDate, SF_capacity,
                                           orgId, pssId,
                                           weather_forecast_source)%>% dplyr::filter(
                                             as.Date(Datatime) == FxDate);
  if(is.null( input)){return(0)}
  else if(nrow(input) != num_blocks)return(0)
  }
  
  avc_df = get_available_cap( nodalId, projType, orgId, pssId, StartDate,EndDate)
  if(is.null( avc_df)){return(6)}
  
  DSM_rate = get_DSM_Rate(nodalId,orgId, pssId)
  if(is.null( DSM_rate)){return(7)}
  
  lat_long = get_lat_long( nodalId, orgId, pssId,projType)
  if(is.null( lat_long)){return(8)}
  
  if(nodalId == 432){
    rt_avc_df = get_available_cap_tetratech(conn, nodalId, FxDate)
    if(is.null( rt_avc_df)){return(9)}
    
  }
  # sunrise_sunset = sunrise_set (FxDate, lat, long)
  # if(is.null( sunrise_sunset)){return(10)}
  
  return(1)
  
}

### test data for Random forest model
get_dayahead_intrday_forecast_Rforest = function(FxDate,reqdf_darksky,
                                                 reqdf_RAD){
  
  reqdf_rad = reqdf_RAD %>% dplyr::filter(date(DATA_DATETIME)==FxDate)
  ### Darksky data ####### 
  reqdf_dark = reqdf_darksky %>% dplyr::filter(date(DATA_DATETIME)==FxDate)
  reqdf_dark = reqdf_dark %>% dplyr :: select(DATA_DATETIME,BLOCK_ID,WIND_SPEED,
                                              WIND_GUST,PRECIP_INTENSITY,AIR_TEMP,
                                              HUMIDITY,CLOUD_COVER,SURFACE_PRESSURE)
  colnames(reqdf_dark) = c("DATA_DATETIME","BLOCK_ID","WIND_SPEED","WIND_GUST",
                           "PRECIP_INTENSITY","AIR_TEMP",
                           "HUMIDITY","CLOUD_COVER","SURFACE_PRESSURE")
  reqdf_dark = merge(reqdf_dark,reqdf_rad,by= c("BLOCK_ID","DATA_DATETIME"),
                     all.x = TRUE)%>%  dplyr :: arrange(BLOCK_ID)
  return(reqdf_dark)
}

### get aggregatedd actual power data provided by CLIENT //// ############
get_n_days_Actdata = function(nodalId,orgId,pssId,FxDate,num_days,
                              SF_capacity,StartDate,EndDate,df_master){
  if(exists("call_data_once") && call_data_once==1){
    actual_power = .GlobalEnv$powerdf_global
    
  }
  else if(!is.data.frame(df_master) || nrow(df_master)<3){
    actual_power= get_30_days_actual_data(nodalId, FxDate, SF_capacity, orgId, pssId,
                                   num_days,StartDate, EndDate)
  }
  else{
    actual_power = df_master
  }
  # actual_power = actual_power[!duplicated(actual_power[,colnames(actual_power)]),]
  # actual_power = as.data.frame(remove_const_zero_days(actual_power,nodalId, projType, orgId,pssId,
  
  actual_power$Datatime = as.character(actual_power$Datatime)
  recent_date_local = as.Date(FxDate)-num_days; # do correct 30 day subtraction
  recent_date1 = as.character(recent_date_local)
  end_date = as.Date(FxDate) - 1
  actual_power = actual_power %>% dplyr::filter(date(Datatime)>=recent_date1 & date(Datatime)<=end_date)
  if (!("FARM_ID" %in% colnames(actual_power))){
    actual_power = actual_power %>% 
      dplyr::mutate(FARM_ID = nodalId)
  }
  ##### get histirical actual data 
  actual_power = actual_power %>% dplyr:: filter(date(Datatime)< FxDate) 
  actual_power = actual_power %>% dplyr :: select(FARM_ID,index1,Datatime,block_id,Power) %>%  
    dplyr::filter(date(Datatime)< FxDate) %>%  dplyr::arrange(Datatime)
  colnames(actual_power) = c("nodalId", "index1", "Datatime","block_id", "Power")
  # close(conn)
  return(actual_power)
}

############ function for Random forest to call Fxdate data,Ensemble model
get_da_intrday_forecast_Rforest =  function(FxDate,reqdf_darksky,
                                            weather_df){
  ## radiation data of FxDate
  reqdf_rad = weather_df %>% dplyr:: filter(date(DATA_DATETIME)== as.Date(FxDate))
  ## Darksky data
  reqdf_dark = reqdf_darksky %>% dplyr:: filter(date(DATA_DATETIME)== as.Date(FxDate))
  ## merge radiation data and darsky data
  reqdf_dark = merge(reqdf_dark,reqdf_rad,by= c("BLOCK_ID","DATA_DATETIME"),
                     all.x = TRUE)%>%  dplyr :: arrange(BLOCK_ID)
  return(reqdf_dark)
}
