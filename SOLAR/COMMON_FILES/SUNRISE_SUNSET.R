#new logic for sunrise sunset to be used every where i.e. gamblr, random forest

sunrise_set = function(df_master, nodalId){
  
  zero_corrected = SF_capacity*min_act_factor*1000 # in KW
  if(nodalId==816 | nodalId==613){
    start = 1
    end = 96
  }else{
    #default value for sunrise and sunset
    start = 23
    end = 78
    res = get_7_days_data(df_master, FxDate)
    if(is.data.frame(res) && nrow(res)>96){
      start = 0
      end = 0
      length = 0
      for(my_date in unique(as.Date(res$Datatime))){
        df = res %>% dplyr::filter(as.Date(Datatime)==as.Date(my_date))
        blocks <- 22:80
        check = blocks %in% df$block_id 
        
        if(all(check)){
          df = df %>%filter(Power>zero_corrected)
          temp_start = min(df$block_id)
          temp_end = max(df$block_id)
          if(!is.na(temp_start) && !is.na(temp_end) && temp_start>=22 && temp_start<33 && temp_end>64 && temp_end<80){
            start = start+temp_start
            end = end+temp_end
            length = length+1
          }
        }
      }
      start = floor(start/length)-1
      end = ceiling(end/length)+1
      if(is.na(start) | is.na(end))
      {
        start = 23
        end=78
      }
    } 
    
  }
  sunrise <<- start
  sunset <<- end
  return(c(start,end))
}

# #Add comments
# 
# tz = 5.5; #in clear sky sheet, for India
# #tz = -7    
# rad = 180/pi
# local_time = "12:00:00"  # fixed
# 
# sunrise_set = function (FxDate, lat, long){#,
#   
#   #print (c(lat, long))
#   B2= lat; B3 = long; B4 = tz; B5 = local_time ; B6 = year(FxDate[1])
#   
#   FxDate_date = ymd_hms(paste(FxDate, B5, sep=" "))
#   tz(FxDate_date) = "America/Los_Angeles"
#   year1 = year(FxDate_date); month1 = month(FxDate_date); day1=day(FxDate_date); hour1 = hour(FxDate_date); min1 = minute(FxDate_date); sec1 = second(FxDate_date) 
#   sunrise_df = data.frame(FxDate)
#   colnames(sunrise_df) = "D"  
#   sunrise_df$D = as.Date(sunrise_df$D)
#   sunrise_df$E = B5   
#   sunrise_df = sunrise_df %>%  
#     mutate(F1 = julianDay(FxDate, year = year1, month= month1, day=day1, hour = hour1, min = min1, sec = sec1, tz = "America/Los_Angeles"),      #D + 2415018.5 + E - B4/24, 
#            G  = (F1-2451545)/36525,
#            I = (280.46646 + G*(36000.76983 + G*0.0003032)) %% 360,
#            J = 357.52911 + G*(35999.05029 - 0.0001537*G),
#            K = 0.016708634 - G*(0.000042037 + 0.0000001267*G),
#            L = sin(rad(J))*(1.914602 - G*(0.004817+0.000014*G)) + sin(rad(2*J))*(0.019993-0.000101*G) + sin(rad(3*J))*0.000289,
#            M = I + L,
#            N = J + L,
#            O = (1.000001018*(1-K*K))/(1+K*cos(rad(N))),
#            P = M-0.00569-0.00478*sin(rad(125.04-1934.136*G)),
#            Q = 23 + (26 + ((21.448 - G*(46.815 + G*(0.00059 - G*0.001813))))/60)/60,
#            R = Q + 0.00256*cos(rad(125.04-1934.136*G)),
#            S = deg(atan2( cos(rad(R))*sin(rad(P)), cos(rad(P)) )), #atan2(y,x), whereas in excel atan2(x,y), so changed the order
#            T1 = deg(asin(sin(rad(R))*sin(rad(P)))),
#            U = tan(rad(R/2))*tan(rad(R/2)),
#            V = 4*deg(U*sin(2*rad(I))-2*K*sin(rad(J))+4*K*U*sin(rad(J))*cos(2*rad(I))-0.5*U*U*sin(4*rad(I))-1.25*K*K*sin(2*rad(J))),
#            W = deg(acos(cos(rad(90.833))/(cos(rad(B2))*cos(rad(T1)))-tan(rad(B2))*tan(rad(T1)))),
#            X = chron::times((720-4*B3-V+B4*60)/1440),
#            Y = chron::times((X*1440-W*4)/1440),
#            Z = chron::times((X*1440+W*4)/1440)
#     )
#   sunrise = sunrise_df$Y
#   sunset = sunrise_df$Z
#   
#   return(c(sunrise, sunset))
#   
# }
# 
# #sunrise_set(FxDate, lat, long)
