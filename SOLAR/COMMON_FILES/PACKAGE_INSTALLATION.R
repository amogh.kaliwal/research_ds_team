Package_Installation <- function(packages){
  
  newPackages <- packages[!(packages %in% 
                              installed.packages()[ , "Package"])]
  
  if (length(newPackages) != 0){
    install.packages(pkgs = newPackages,lib="/usr/lib/R/library",
                     dependencies = TRUE)
    sapply(X = packages, FUN = require, character.only = TRUE)
  } else {
    sapply(X = packages, FUN = require, character.only = TRUE)
  }
}

Package_Installation(c("ggplot2","ggthemes","rpart","randomForest","mlbench",
                       "e1071","Hmisc","zoo","forecast","stringr",
                       "glmnet","mlr","plotly", "imputeTS", "sp", "jsonlite", 
                       "httr", "purrr", "NCmisc", "wakefield", "gWidgets2",
                       "RestRserve", "RCurl", "CircStats", "oce", "dplyr",
                       "lubridate"))

