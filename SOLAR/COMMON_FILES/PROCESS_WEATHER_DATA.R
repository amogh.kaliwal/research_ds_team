############### To get pre-processed Weather Data ####################
preprocess_data_1 = function(reqdf){
  reqdf$DATA_DATETIME = as.character(reqdf$DATA_DATETIME)
  reqdf = reqdf %>% dplyr:: arrange(DATA_DATETIME)
  reqdf$DATA_DATETIME = as.character(reqdf$DATA_DATETIME)
  return(reqdf)
}

preprocess_data_2 = function(reqdf){
  reqdf$DATA_DATETIME = as.character(reqdf$DATA_DATETIME)
  reqdf$ENTRY_TIME = as.character(reqdf$ENTRY_TIME)
  reqdf = reqdf %>% dplyr:: mutate(TIME_DIFF = difftime(DATA_DATETIME,ENTRY_TIME,unit="hours")) %>%
    filter(TIME_DIFF > 0) %>% group_by(DATA_DATETIME) %>%
    dplyr :: slice_min((TIME_DIFF)) %>%dplyr:: select(-TIME_DIFF)
  
  reqdf = data.frame(reqdf)
  reqdf = reqdf %>% dplyr:: arrange(DATA_DATETIME)
  reqdf$DATA_DATETIME = as.character(reqdf$DATA_DATETIME)
  return(reqdf)
}

preprocess_data_3 = function(reqdf){
  reqdf$DATA_DATETIME = as.character(reqdf$DATA_DATETIME)
  #reqdf$ENTRY_TIME = as.character(reqdf$ENTRY_TIME)
  reqdf = as.data.frame(reqdf %>% group_by(DATA_DATETIME) %>%
                          dplyr :: slice_max(ENTRY_TIME))
  reqdf = data.frame(reqdf)
  reqdf = reqdf %>% dplyr:: arrange(DATA_DATETIME)
  reqdf$DATA_DATETIME = as.character(reqdf$DATA_DATETIME)
  return(reqdf)
}

get_ws_error_with_preprocess_data_1 = function(reqdf){
  #This function should be used for wsid: meteobias, extrems_ws
  
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    reqdf = preprocess_data_1(reqdf)
  }
  return(reqdf)
}

get_ws_error_with_preprocess_data_2 = function(reqdf){
  #This function should be used for wsid: Persistence_ws,NCEP_CLOUDBIAS,ensemble2_ws,
  #metdesk_demo,normet_ws,ncep_gfs_bias,weather_data,IITM_solar,solcast,NCEP_GFS_INDONESIA,
  #NOIDA_4KM.
  
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
    return(reqdf)
  }else{
    ws_error <<-0
    reqdf = preprocess_data_2(reqdf)
  }
  return(reqdf)
}

get_ws_error_with_preprocess_data_3 = function(reqdf){
  #This function should be used for wsid: NCEP_NON_DS,NCMRWF_12_ws,realtime_ws,
  #ensemble_ws,ECMWF_ws
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    #message("Using NCEP NON DS")
    reqdf = preprocess_data_3(reqdf)
  }
  return(reqdf)
}


get_pixel_ws = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    #message("Using Image Processing data")               
  }
  return(reqdf)
}

get_backup_ws = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    reqdf$DATATIME = as.character(reqdf$DATATIME)
    #reqdf$ENTRY_TIME = as.character(reqdf$ENTRY_TIME)
    reqdf = as.data.frame(reqdf %>% group_by(DATATIME) %>%
                            dplyr :: slice_max(ENTRY_TIME))
    reqdf = data.frame(reqdf)
    reqdf = reqdf %>% dplyr:: arrange(DATATIME)
    reqdf$DATATIME = as.character(reqdf$DATATIME)
    #message("Using  BACK UP WEATHER SOURCE")
  }
  return(reqdf)
}


get_ncep_gfs_indonesia_bias = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    #Select the latest of the 0z/12z file
    reqdf = as.data.frame(reqdf %>% group_by(DATA_DATETIME) %>%
                            dplyr :: slice_max(ENTRY_TIME))
    #message("Using  NCEP_GFS_INDONESIA_BIAS_CORRECTED Data")
  }
  return(reqdf)
}

get_IMD_GFS = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<-0
  }
  return(reqdf)
}

get_NCEP_GFS = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    if ('DATA_DATETIME' %in% colnames(reqdf)){
      reqdf = rename(reqdf, DATATIME = DATA_DATETIME)
    }
    reqdf = reqdf %>% dplyr:: mutate(TIME_DIFF = difftime(DATATIME,ENTRY_TIME,unit="hours")) %>%
      filter(TIME_DIFF > 0) %>% group_by(DATATIME) %>%
      dplyr :: slice(which.min(TIME_DIFF)) %>%dplyr:: select(-TIME_DIFF)
    
    reqdf = reqdf %>% dplyr:: arrange(DATATIME)
    #message(" NCEP GFS data used")
    
  }
  return(reqdf)
}

get_Meteodata = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0
    reqdf = preprocess_data_2(reqdf)
    meteo_LB_ID = reqdf$BLOCK_ID[nrow(reqdf)]
    #message("Meteotest data used")
  }
  return(reqdf)
}

get_Noidadata = function(reqdf){
  if(!is.data.frame(reqdf)){
    ws_error <<- 1
  }else{
    ws_error <<- 0 
    reqdf$Date = as.Date(reqdf$DATA_DATETIME)
    reqdf = preprocess_data_2(reqdf)
    #message("NOIDA data used")
    if ("CLOUD_COVER" %in% colnames(reqdf)){
      reqdf = reqdf %>% dplyr::select(-CLOUD_COVER)
    }
  }
  return(reqdf)
}
