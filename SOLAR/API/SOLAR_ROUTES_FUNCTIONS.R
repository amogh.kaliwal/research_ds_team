getsimplesolar <- function(request, response){
  
  if((is.null(request$parameters_query[["fxdate"]])|is.null(request$parameters_query[["wfxid"]])|
      is.null(request$parameters_query[["nodalid"]])|is.null(request$parameters_query[["fxtype"]])|
      is.null(request$parameters_query[["orgid"]])|is.null(request$parameters_query[["pssid"]])|
      is.null(request$parameters_query[["blkid"]])|is.null(request$parameters_query[["cmfx"]] )|
      is.null(request$parameters_query[["ndays"]]) | is.null(request$parameters_query[["gran"]]) |
      is.null(request$parameters_query[["isAutoFx"]]))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"One of the parameter passed as NA",'"}'))
    
  }else if(!is.character(request$parameters_query[["fxdate"]])){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate should be a charecter type",'"}'))
    
  }else if(!nchar(request$parameters_query[["fxdate"]]) ==10){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate format is not correct",'"}'))
    
  }else if(as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))>12|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))>31|
           as.numeric(substr(request$parameters_query[["fxdate"]], 1,1))==0){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else if(gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][1]!=5|
           gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][2]!=8 ){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else{
                     FxDate <<- as.Date(request$parameters_query[["fxdate"]])
    weather_forecast_source <<- as.integer(request$parameters_query[["wfxid"]])
                    nodalId <<- as.integer(request$parameters_query[["nodalid"]])
          dayahead_intraday <<- as.integer(request$parameters_query[["fxtype"]])
                      orgId <<- as.integer(request$parameters_query[["orgid"]])
                      pssId <<- as.integer(request$parameters_query[["pssid"]])
                  FxBlockID <<- as.integer(request$parameters_query[["blkid"]])
              cumulative_fx <<- as.integer(request$parameters_query[["cmfx"]])
                   nfx_days <<- as.integer(request$parameters_query[["ndays"]])
                granularity <<- as.integer(request$parameters_query[["gran"]])
                  isAutoFx <<- as.integer(request$parameters_query[["isAutoFx"]])
    
  }
  
  if((length(FxDate) == 0L | is.na(FxDate)) | 
     (length(weather_forecast_source) == 0L | is.na(weather_forecast_source)) |
     (length(nodalId) == 0L | is.na(nodalId)) |
     (length(dayahead_intraday) == 0L | is.na(dayahead_intraday)) | 
     (length(orgId) == 0L | is.na(orgId)) |
     (length(pssId) == 0L | is.na(pssId)) |
     (length(FxBlockID) == 0L | is.na(FxBlockID)) |
     (length(cumulative_fx) == 0L | is.na(cumulative_fx)) |
     (length(nfx_days) == 0L | is.na(nfx_days)) |
     (length(granularity) == 0L | is.na(granularity)) |
     (length(isAutoFx) == 0L | is.na(isAutoFx))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"Invalid input parameters entered",'"}'))
  }
  
  model_name <<- "SIMPLE_SOLAR"
  source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR_SOURCE_FILES.R",sep = "/"))
  model_num <- find_model_num(model_name)
  library(zeallot)
  start_tm = Sys.time()
  c(SF_df,StartDate,EndDate,num_days,avc_df,act_df,weather_df) %<-% basic_data_avaliabily_check(orgId,pssId,nodalId,FxDate,weather_forecast_source,
                                                                                                FxBlockID,dayahead_intraday,isAutoFx,model_num,model_class=0)
  
  function_get_log(msg="basic_data_avaliabily_check processed",start_tm,df = data.frame(),FxDate,pssId,nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  detach("package:zeallot", unload = TRUE)
  val_out = val_par_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,
                         dayahead_intraday,cumulative_fx,nfx_days,granularity,StartDate,
                         EndDate,num_days,weather_df,avc_df)
  message("Val_out: ", val_out)
  if(val_out == 1){
    source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
    msg <- "Forecast generated successfully"
    result <- main_simple_solar_full_day_forecast(nodalId, FxDate, FxBlockID,weather_forecast_source, 
                                                  dayahead_intraday,orgId,pssId,cumulative_fx,weather_df,
                                                  avc_df,msg)
  }else{
    result <- val_out_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source, 
                           dayahead_intraday,cumulative_fx,StartDate,EndDate,num_days,weather_df, 
                           avc_df,SF_capacity,val_out)
  }
  result_df = as.data.frame(result)
  function_get_log(msg = "result json df", start_tm, df = result_df, FxDate, pssId, nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  finalResult <- result #toJSON(result, dataframe = "rows")
  response$set_body(finalResult)
  response$set_content_type("text/plain")
}           

#######################

getrandomforest <-  function(request, response){
  
  if((is.null(request$parameters_query[["fxdate"]])|is.null(request$parameters_query[["wfxid"]])|
      is.null(request$parameters_query[["nodalid"]])|is.null(request$parameters_query[["fxtype"]])|
      is.null(request$parameters_query[["orgid"]])|is.null(request$parameters_query[["pssid"]])|
      is.null(request$parameters_query[["blkid"]])|is.null(request$parameters_query[["cmfx"]] )|
      is.null(request$parameters_query[["ndays"]]) | is.null(request$parameters_query[["gran"]]) |
      is.null(request$parameters_query[["isAutoFx"]]))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"One of the parameter passed as NA",'"}'))
    
  }else if(!is.character(request$parameters_query[["fxdate"]])){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate should be a charecter type",'"}'))
    
  }else if(!nchar(request$parameters_query[["fxdate"]]) ==10){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate format is not correct",'"}'))
    
  }else if(as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))>12|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))>31|
           as.numeric(substr(request$parameters_query[["fxdate"]], 1,1))==0){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else if(gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][1]!=5|
           gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][2]!=8 ){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else{
    FxDate <<- as.Date(request$parameters_query[["fxdate"]])
    weather_forecast_source <<- as.integer(request$parameters_query[["wfxid"]])
    nodalId <<- as.integer(request$parameters_query[["nodalid"]])
    dayahead_intraday <<- as.integer(request$parameters_query[["fxtype"]])
    orgId <<- as.integer(request$parameters_query[["orgid"]])
    pssId <<- as.integer(request$parameters_query[["pssid"]])
    FxBlockID <<- as.integer(request$parameters_query[["blkid"]])
    cumulative_fx <<- as.integer(request$parameters_query[["cmfx"]])
    nfx_days <<- as.integer(request$parameters_query[["ndays"]])
    granularity <<- as.integer(request$parameters_query[["gran"]])
    isAutoFx <<- as.integer(request$parameters_query[["isAutoFx"]])
    
  }
  
  if((length(FxDate) == 0L | is.na(FxDate)) | 
     (length(weather_forecast_source) == 0L | is.na(weather_forecast_source)) |
     (length(nodalId) == 0L | is.na(nodalId)) |
     (length(dayahead_intraday) == 0L | is.na(dayahead_intraday)) | 
     (length(orgId) == 0L | is.na(orgId)) |
     (length(pssId) == 0L | is.na(pssId)) |
     (length(FxBlockID) == 0L | is.na(FxBlockID)) |
     (length(cumulative_fx) == 0L | is.na(cumulative_fx)) |
     (length(nfx_days) == 0L | is.na(nfx_days)) |
     (length(granularity) == 0L | is.na(granularity)) |
     (length(isAutoFx) == 0L | is.na(isAutoFx))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"Invalid input parameters entered",'"}'))
  }
  model_name <<- "RANDOM_FOREST"
  source(paste(getwd(),"SOLAR/MODELS/RANDOM_FOREST/RANDOM_FOREST_SOURCE_FILES.R",sep = "/"))
  model_num <- find_model_num(model_name)
  rf_path = model_path_function(FxDate,nodalId,pssId,orgId,weather_forecast_source,dayahead_intraday,extra=0)[2]
  if(length(list.files(rf_path))>=9){
    model_class <<-'rf'
  }else{
    model_class <<-0
  }
  library(zeallot)
  start_tm = Sys.time()
  c(SF_df,StartDate,EndDate,num_days,avc_df,act_df,weather_df) %<-% basic_data_avaliabily_check(orgId,pssId,nodalId,FxDate,weather_forecast_source,
                                                                                                FxBlockID,dayahead_intraday,isAutoFx,model_num,model_class)
  
  function_get_log(msg="basic_data_avaliabily_check processed",start_tm,df = data.frame(),FxDate,pssId,nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  detach("package:zeallot", unload = TRUE)
  
  reqdf_darksky = get_agg_darksky_data(nodalId,orgId,pssId,FxDate,num_days,dayahead_intraday)
  
  val_out = val_par_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,
                         dayahead_intraday,cumulative_fx,nfx_days,granularity,StartDate,
                         EndDate,num_days,weather_df,avc_df,act_df,reqdf_darksky)
  message("Val_out: ", val_out)
  
  if(val_out == 1){
    source(paste(getwd(),"SOLAR/MODELS/RANDOM_FOREST/RANDOM_FOREST.R",sep = "/"))
    result <- main_solar_forecast_fullday_Randomforest(nodalId,FxDate,FxBlockID,weather_forecast_source,
                                                       dayahead_intraday, orgId,pssId,cumulative_fx,
                                                       weather_df,avc_df,act_df,reqdf_darksky,model_class)
  }else{
    result <- val_out_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,dayahead_intraday
                           ,cumulative_fx,StartDate,EndDate,num_days,weather_df,avc_df,SF_capacity,val_out)
  }
  finalResult <- result #toJSON(result, dataframe = "rows")
  response$set_body(finalResult)
  response$set_content_type("text/plain")
}

#######################

getpersistence1 <- function(request, response){
  
  if((is.null(request$parameters_query[["fxdate"]])|is.null(request$parameters_query[["wfxid"]])|
      is.null(request$parameters_query[["nodalid"]])|is.null(request$parameters_query[["fxtype"]])|
      is.null(request$parameters_query[["orgid"]])|is.null(request$parameters_query[["pssid"]])|
      is.null(request$parameters_query[["blkid"]])|is.null(request$parameters_query[["cmfx"]] )|
      is.null(request$parameters_query[["ndays"]]) | is.null(request$parameters_query[["gran"]]) |
      is.null(request$parameters_query[["isAutoFx"]]))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"One of the parameter passed as NA",'"}'))
    
  }else if(!is.character(request$parameters_query[["fxdate"]])){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate should be a charecter type",'"}'))
    
  }else if(!nchar(request$parameters_query[["fxdate"]]) ==10){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate format is not correct",'"}'))
    
  }else if(as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))>12|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))>31|
           as.numeric(substr(request$parameters_query[["fxdate"]], 1,1))==0){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else if(gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][1]!=5|
           gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][2]!=8 ){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else{
    FxDate <<- as.Date(request$parameters_query[["fxdate"]])
    weather_forecast_source <<- as.integer(request$parameters_query[["wfxid"]])
    nodalId <<- as.integer(request$parameters_query[["nodalid"]])
    dayahead_intraday <<- as.integer(request$parameters_query[["fxtype"]])
    orgId <<- as.integer(request$parameters_query[["orgid"]])
    pssId <<- as.integer(request$parameters_query[["pssid"]])
    FxBlockID <<- as.integer(request$parameters_query[["blkid"]])
    cumulative_fx <<- as.integer(request$parameters_query[["cmfx"]])
    nfx_days <<- as.integer(request$parameters_query[["ndays"]])
    granularity <<- as.integer(request$parameters_query[["gran"]])
    isAutoFx <<- as.integer(request$parameters_query[["isAutoFx"]])
    
  }
  
  if((length(FxDate) == 0L | is.na(FxDate)) | 
     (length(weather_forecast_source) == 0L | is.na(weather_forecast_source)) |
     (length(nodalId) == 0L | is.na(nodalId)) |
     (length(dayahead_intraday) == 0L | is.na(dayahead_intraday)) | 
     (length(orgId) == 0L | is.na(orgId)) |
     (length(pssId) == 0L | is.na(pssId)) |
     (length(FxBlockID) == 0L | is.na(FxBlockID)) |
     (length(cumulative_fx) == 0L | is.na(cumulative_fx)) |
     (length(nfx_days) == 0L | is.na(nfx_days)) |
     (length(granularity) == 0L | is.na(granularity)) |
     (length(isAutoFx) == 0L | is.na(isAutoFx))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"Invalid input parameters entered",'"}'))
  }
  
  model_name <<- "PERSISTENCE1"
  source(paste(getwd(),"SOLAR/MODELS/PERSISTENCE1/PERSISTENCE1_SOURCE_FILES.R",sep = "/"))
  model_num <- find_model_num(model_name)
  library(zeallot)
  start_tm = Sys.time()
  c(SF_df,StartDate,EndDate,num_days,avc_df,act_df,weather_df) %<-% basic_data_avaliabily_check(orgId,pssId,nodalId,FxDate,weather_forecast_source,
                                                                                                FxBlockID,dayahead_intraday,isAutoFx,model_num,model_class=0)
  
  function_get_log(msg="basic_data_avaliabily_check processed",start_tm,df = data.frame(),FxDate,pssId,nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  detach("package:zeallot", unload = TRUE)
  val_out = val_par_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,
                         dayahead_intraday,cumulative_fx,nfx_days,granularity,StartDate,
                         EndDate,num_days,weather_df,avc_df,act_df)
  message("Val_out: ", val_out)
  if(val_out == 1){
    source(paste(getwd(),"SOLAR/MODELS/PERSISTENCE1/PERSISTENCE1.R",sep = "/"))
    msg <- "Forecast generated successfully"
    result <- main_persistence1_forecast(nodalId,FxDate,FxBlockID,weather_forecast_source,
                                         dayahead_intraday,orgId,pssId,StartDate,EndDate,num_days
                                         ,weather_df,avc_df,act_df)
  }else{
    result <- val_out_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source, 
                           dayahead_intraday,cumulative_fx,StartDate,EndDate,num_days,weather_df, 
                           avc_df,SF_capacity,val_out)
  }
  result_df = as.data.frame(result)
  function_get_log(msg = "result json df", start_tm, df = result_df, FxDate, pssId, nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  finalResult <- result #toJSON(result, dataframe = "rows")
  response$set_body(finalResult)
  response$set_content_type("text/plain")
}  

#######################

getpersistence2 <- function(request, response) {
  
  if((is.null(request$parameters_query[["fxdate"]])|is.null(request$parameters_query[["wfxid"]])|
      is.null(request$parameters_query[["nodalid"]])|is.null(request$parameters_query[["fxtype"]])|
      is.null(request$parameters_query[["orgid"]])|is.null(request$parameters_query[["pssid"]])|
      is.null(request$parameters_query[["blkid"]])|is.null(request$parameters_query[["cmfx"]] )|
      is.null(request$parameters_query[["ndays"]]) | is.null(request$parameters_query[["gran"]]) |
      is.null(request$parameters_query[["isAutoFx"]]))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"One of the parameter passed as NA",'"}'))
    
  }else if(!is.character(request$parameters_query[["fxdate"]])){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate should be a charecter type",'"}'))
    
  }else if(!nchar(request$parameters_query[["fxdate"]]) ==10){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate format is not correct",'"}'))
    
  }else if(as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))>12|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))>31|
           as.numeric(substr(request$parameters_query[["fxdate"]], 1,1))==0){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else if(gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][1]!=5|
           gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][2]!=8 ){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else{
    FxDate <<- as.Date(request$parameters_query[["fxdate"]])
    weather_forecast_source <<- as.integer(request$parameters_query[["wfxid"]])
    nodalId <<- as.integer(request$parameters_query[["nodalid"]])
    dayahead_intraday <<- as.integer(request$parameters_query[["fxtype"]])
    orgId <<- as.integer(request$parameters_query[["orgid"]])
    pssId <<- as.integer(request$parameters_query[["pssid"]])
    FxBlockID <<- as.integer(request$parameters_query[["blkid"]])
    cumulative_fx <<- as.integer(request$parameters_query[["cmfx"]])
    nfx_days <<- as.integer(request$parameters_query[["ndays"]])
    granularity <<- as.integer(request$parameters_query[["gran"]])
    isAutoFx <<- as.integer(request$parameters_query[["isAutoFx"]])
    
  }
  
  if((length(FxDate) == 0L | is.na(FxDate)) | 
     (length(weather_forecast_source) == 0L | is.na(weather_forecast_source)) |
     (length(nodalId) == 0L | is.na(nodalId)) |
     (length(dayahead_intraday) == 0L | is.na(dayahead_intraday)) | 
     (length(orgId) == 0L | is.na(orgId)) |
     (length(pssId) == 0L | is.na(pssId)) |
     (length(FxBlockID) == 0L | is.na(FxBlockID)) |
     (length(cumulative_fx) == 0L | is.na(cumulative_fx)) |
     (length(nfx_days) == 0L | is.na(nfx_days)) |
     (length(granularity) == 0L | is.na(granularity)) |
     (length(isAutoFx) == 0L | is.na(isAutoFx))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"Invalid input parameters entered",'"}'))
  }
  
  model_name <<- "PERSISTENCE2"
  source(paste(getwd(),"SOLAR/MODELS/PERSISTENCE2/PERSISTENCE2_SOURCE_FILES.R",sep = "/"))
  model_num <- find_model_num(model_name)
  library(zeallot)
  start_tm = Sys.time()
  c(SF_df,StartDate,EndDate,num_days,avc_df,act_df,weather_df) %<-% basic_data_avaliabily_check(orgId,pssId,nodalId,FxDate,weather_forecast_source,
                                                                                                FxBlockID,dayahead_intraday,isAutoFx,model_num,model_class=0)
  
  function_get_log(msg="basic_data_avaliabily_check processed",start_tm,df = data.frame(),FxDate,pssId,nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  detach("package:zeallot", unload = TRUE)
  val_out = val_par_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,
                         dayahead_intraday,cumulative_fx,nfx_days,granularity,StartDate,
                         EndDate,num_days,weather_df,avc_df,act_df)
  message("Val_out: ", val_out)
  if(val_out == 1){
    source(paste(getwd(),"SOLAR/MODELS/PERSISTENCE2/PERSISTENCE2.R",sep = "/"))
    msg <- "Forecast generated successfully"
    result <- main_persistence1_forecast(nodalId,FxDate,FxBlockID,weather_forecast_source,
                                         dayahead_intraday,orgId,pssId,StartDate,EndDate,num_days
                                         ,weather_df,avc_df,act_df)
  }else{
    result <- val_out_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source, 
                           dayahead_intraday,cumulative_fx,StartDate,EndDate,num_days,weather_df, 
                           avc_df,SF_capacity,val_out)
  }
  result_df = as.data.frame(result)
  function_get_log(msg = "result json df", start_tm, df = result_df, FxDate, pssId, nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  finalResult <- result #toJSON(result, dataframe = "rows")
  response$set_body(finalResult)
  response$set_content_type("text/plain")
}

#####################

getfactorsolar <- function(request, response){
  
  if((is.null(request$parameters_query[["fxdate"]])|is.null(request$parameters_query[["wfxid"]])|
      is.null(request$parameters_query[["nodalid"]])|is.null(request$parameters_query[["fxtype"]])|
      is.null(request$parameters_query[["orgid"]])|is.null(request$parameters_query[["pssid"]])|
      is.null(request$parameters_query[["blkid"]])|is.null(request$parameters_query[["cmfx"]] )|
      is.null(request$parameters_query[["ndays"]]) | is.null(request$parameters_query[["gran"]]) |
      is.null(request$parameters_query[["isAutoFx"]]))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"One of the parameter passed as NA",'"}'))
    
  }else if(!is.character(request$parameters_query[["fxdate"]])){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate should be a charecter type",'"}'))
    
  }else if(!nchar(request$parameters_query[["fxdate"]]) ==10){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate format is not correct",'"}'))
    
  }else if(as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))>12|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))>31|
           as.numeric(substr(request$parameters_query[["fxdate"]], 1,1))==0){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else if(gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][1]!=5|
           gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][2]!=8 ){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else{
    FxDate <<- as.Date(request$parameters_query[["fxdate"]])
    weather_forecast_source <<- as.integer(request$parameters_query[["wfxid"]])
    nodalId <<- as.integer(request$parameters_query[["nodalid"]])
    dayahead_intraday <<- as.integer(request$parameters_query[["fxtype"]])
    orgId <<- as.integer(request$parameters_query[["orgid"]])
    pssId <<- as.integer(request$parameters_query[["pssid"]])
    FxBlockID <<- as.integer(request$parameters_query[["blkid"]])
    cumulative_fx <<- as.integer(request$parameters_query[["cmfx"]])
    nfx_days <<- as.integer(request$parameters_query[["ndays"]])
    granularity <<- as.integer(request$parameters_query[["gran"]])
    isAutoFx <<- as.integer(request$parameters_query[["isAutoFx"]])
    
  }
  
  if((length(FxDate) == 0L | is.na(FxDate)) | 
     (length(weather_forecast_source) == 0L | is.na(weather_forecast_source)) |
     (length(nodalId) == 0L | is.na(nodalId)) |
     (length(dayahead_intraday) == 0L | is.na(dayahead_intraday)) | 
     (length(orgId) == 0L | is.na(orgId)) |
     (length(pssId) == 0L | is.na(pssId)) |
     (length(FxBlockID) == 0L | is.na(FxBlockID)) |
     (length(cumulative_fx) == 0L | is.na(cumulative_fx)) |
     (length(nfx_days) == 0L | is.na(nfx_days)) |
     (length(granularity) == 0L | is.na(granularity)) |
     (length(isAutoFx) == 0L | is.na(isAutoFx))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"Invalid input parameters entered",'"}'))
  }
  
  model_name <<- "FACTOR_SOLAR"
  source(paste(getwd(),"SOLAR/MODELS/FACTOR_SOLAR/FACTOR_SOLAR_SOURCE_FILES.R",sep = "/"))
  model_num <- find_model_num(model_name)
  library(zeallot)
  start_tm = Sys.time()
  c(SF_df,StartDate,EndDate,num_days,avc_df,act_df,weather_df) %<-% basic_data_avaliabily_check(orgId,pssId,nodalId,FxDate,weather_forecast_source,
                                                                                                FxBlockID,dayahead_intraday,isAutoFx,model_num,model_class=0)
  
  function_get_log(msg="basic_data_avaliabily_check processed",start_tm,df = data.frame(),FxDate,pssId,nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  detach("package:zeallot", unload = TRUE)
  val_out = val_par_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,
                         dayahead_intraday,cumulative_fx,nfx_days,granularity,StartDate,
                         EndDate,num_days,weather_df,avc_df,act_df)
  message("Val_out: ", val_out)
  if(val_out == 1){
    source(paste(getwd(),"SOLAR/MODELS/FACTOR_SOLAR/FACTOR_SOLAR.R",sep = "/"))
    msg <- "Forecast generated successfully"
    result <- main_factor_solar_full_day_forecast(nodalId,FxDate,FxBlockID,weather_forecast_source, 
                                                  dayahead_intraday,orgId,pssId,cumulative_fx,weather_df,
                                                  avc_df,act_df,msg,StartDate,EndDate,num_days)
  }else{
    result <- val_out_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source, 
                           dayahead_intraday,cumulative_fx,StartDate,EndDate,num_days,weather_df, 
                           avc_df,SF_capacity,val_out)
  }
  result_df = as.data.frame(result)
  function_get_log(msg = "result json df", start_tm, df = result_df, FxDate, pssId, nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  finalResult <- result #toJSON(result, dataframe = "rows")
  response$set_body(finalResult)
  response$set_content_type("text/plain")
}

#####################

getgamblr <- function(request, response){
  
  if((is.null(request$parameters_query[["fxdate"]])|is.null(request$parameters_query[["wfxid"]])|
      is.null(request$parameters_query[["nodalid"]])|is.null(request$parameters_query[["fxtype"]])|
      is.null(request$parameters_query[["orgid"]])|is.null(request$parameters_query[["pssid"]])|
      is.null(request$parameters_query[["blkid"]])|is.null(request$parameters_query[["cmfx"]] )|
      is.null(request$parameters_query[["ndays"]]) | is.null(request$parameters_query[["gran"]]) |
      is.null(request$parameters_query[["isAutoFx"]]))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"One of the parameter passed as NA",'"}'))
    
  }else if(!is.character(request$parameters_query[["fxdate"]])){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate should be a charecter type",'"}'))
    
  }else if(!nchar(request$parameters_query[["fxdate"]]) ==10){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate format is not correct",'"}'))
    
  }else if(as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 6, 7))>12|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))<1|
           as.numeric(substr(request$parameters_query[["fxdate"]], 9, 10))>31|
           as.numeric(substr(request$parameters_query[["fxdate"]], 1,1))==0){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else if(gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][1]!=5|
           gregexpr(pattern ='-',request$parameters_query[["fxdate"]])[[1]][2]!=8 ){
    
    result = (paste0('{"sts":"FALSE","msg":"',"FxDate is invalid",'"}'))
    
  }else{
    FxDate <<- as.Date(request$parameters_query[["fxdate"]])
    weather_forecast_source <<- as.integer(request$parameters_query[["wfxid"]])
    nodalId <<- as.integer(request$parameters_query[["nodalid"]])
    dayahead_intraday <<- as.integer(request$parameters_query[["fxtype"]])
    orgId <<- as.integer(request$parameters_query[["orgid"]])
    pssId <<- as.integer(request$parameters_query[["pssid"]])
    FxBlockID <<- as.integer(request$parameters_query[["blkid"]])
    cumulative_fx <<- as.integer(request$parameters_query[["cmfx"]])
    nfx_days <<- as.integer(request$parameters_query[["ndays"]])
    granularity <<- as.integer(request$parameters_query[["gran"]])
    isAutoFx <<- as.integer(request$parameters_query[["isAutoFx"]])
    
  }
  
  if((length(FxDate) == 0L | is.na(FxDate)) | 
     (length(weather_forecast_source) == 0L | is.na(weather_forecast_source)) |
     (length(nodalId) == 0L | is.na(nodalId)) |
     (length(dayahead_intraday) == 0L | is.na(dayahead_intraday)) | 
     (length(orgId) == 0L | is.na(orgId)) |
     (length(pssId) == 0L | is.na(pssId)) |
     (length(FxBlockID) == 0L | is.na(FxBlockID)) |
     (length(cumulative_fx) == 0L | is.na(cumulative_fx)) |
     (length(nfx_days) == 0L | is.na(nfx_days)) |
     (length(granularity) == 0L | is.na(granularity)) |
     (length(isAutoFx) == 0L | is.na(isAutoFx))){
    
    result = (paste0('{"sts":"FALSE","msg":"',"Invalid input parameters entered",'"}'))
  }
  
  model_name <<- "GAMBLR"
  source(paste(getwd(),"SOLAR/MODELS/GAMBLR/GAMBLR_SOURCE_FILES.R",sep = "/"))
  model_num <- find_model_num(model_name)
  gamblr_model <<- read_model(nodalId,FxDate,orgId,pssId,weather_forecast_source,
                              model_type,extra=0)
  model_class <<- class(gamblr_model)
  library(zeallot)
  start_tm = Sys.time()
  c(SF_df,StartDate,EndDate,num_days,avc_df,act_df,weather_df) %<-% basic_data_avaliabily_check(orgId,pssId,nodalId,FxDate,weather_forecast_source,
                                                                                                FxBlockID,dayahead_intraday,isAutoFx,model_num,model_class)
  
  function_get_log(msg="basic_data_avaliabily_check processed",start_tm,df = data.frame(),FxDate,pssId,nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  detach("package:zeallot", unload = TRUE)
  val_out = val_par_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source,
                         dayahead_intraday,cumulative_fx,nfx_days,granularity,StartDate,
                         EndDate,num_days,weather_df,avc_df,act_df)
  message("Val_out: ", val_out)
  if(val_out == 1){
    source(paste(getwd(),"SOLAR/MODELS/GAMBLR/GAMBLR.R",sep = "/"))
    result <- main_gamblr_full_day_forecast(orgId,pssId,nodalId,FxDate,FxBlockID,
                                            weather_forecast_source,dayahead_intraday,
                                            model_type,train_forecast,cumulative_fx,StartDate,
                                            EndDate,num_days,weather_df,avc_df,act_df)
  }else{
    result <- val_out_func(orgId,pssId,nodalId,FxDate,FxBlockID,weather_forecast_source, 
                           dayahead_intraday,cumulative_fx,StartDate,EndDate,num_days,weather_df, 
                           avc_df,SF_capacity,val_out)
  }
  finalResult <- result #toJSON(result, dataframe = "rows")
  response$set_body(finalResult)
  response$set_content_type("text/plain")
  
}