#----------------------------------------------------------------------------------
# Main
#----------------------------------------------------------------------------------
formatOutput_p1 <- function(status, msg, data) {
  if (status == TRUE){
    if (nrow(data) >= 1) {
      colnames(data) = c("f_id","ddt","bid","fx", "rad", "temp","wsid","rmt")
      rmt = unique(data$rmt)
      data = data%>%dplyr::select(ddt,bid,fx,rad,temp)
      fxData <- toJSON(data)
      finalOutput <- paste0('{"sts":"TRUE","msg":"',msg,'","rmt":"',rmt,'","fid":"',
                            nodalId,'","wfxid":"',weather_forecast_source,
                            '","pssid":"',pssId,'","orgid":"',orgId,'","fbid":"',
                            FxBlockID,'","fxdt":"',FxDate,'","fxdata":',fxData,'}')
      
    }else{
      finalOutput <- paste0('{"sts":"FALSE","msg":"',"No. of rows of forecast is less than 1",'}')
    }
  }else{
    finalOutput <- paste0('{"sts":"FALSE","msg":"',data,'"}')
  }
  
  return(finalOutput)
}

#-------------------------------------------------------------------------------
p1_preprocessed_fx = function(nodalId, FxDate, FxBlockID,  weather_forecast_source,
                              dayahead_intraday, orgId, pssId,StartDate,EndDate,
                              num_days,input,avc_df,df_master) {
  result = tryCatch(
    {
      df = Persistence1FullDayForecast(nodalId, FxDate, FxBlockID,  weather_forecast_source,
                                       dayahead_intraday, orgId, pssId,StartDate,
                                       EndDate,num_days,input,avc_df,df_master)
      Persistence1FullDayForecast=NULL;
      return(df) 
      
    }, error = function(err){
      return(paste("Error: ", err));
    }, finally = {
      
    }
  )
  return(result)
}

p1_postprocessed_fx = function(nodalId, FxDate, FxBlockID,  weather_forecast_source,
                               dayahead_intraday, orgId, pssId,StartDate,EndDate,
                               num_days,input,avc_df,df_master){
  #Post processing step of Forecast
  start_tm = Sys.time()
  power_df = p1_preprocessed_fx(nodalId, FxDate, FxBlockID,  weather_forecast_source,
                                dayahead_intraday, orgId, pssId,StartDate,
                                EndDate,num_days,input,avc_df,df_master)
  function_get_log(msg = "result p1_preprocessed_fx", start_tm, df = as.data.frame(power_df),FxDate, pssId, nodalId,
                   dayahead_intraday,weather_forecast_source,isAutoFx)
  # -------------------------------------
  #-Check if power_df is dataframe or not
  # -------------------------------------
  
  if(!is.data.frame(power_df)|identical(character(0),power_df)){
    source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
    power_df <- simple_solar_full_day_forecast(nodalId, FxDate, FxBlockID,
                                               weather_forecast_source =1, 
                                               dayahead_intraday, orgId, pssId,
                                               cumulative_fx,input, avc_df)
    
  }else
    if(is.data.frame(power_df) & nrow(power_df)<3){
      source(paste(getwd(),"SOLAR/MODELS/SIMPLE_SOLAR/SIMPLESOLAR.R",sep = "/"))
      power_df <- simple_solar_full_day_forecast(nodalId, FxDate, FxBlockID,
                                                 weather_forecast_source =1, 
                                                 dayahead_intraday, orgId, pssId,
                                                 cumulative_fx,input, avc_df)
      
    }
  
  #Save the fx
  output = power_df
  
  
  ########################################
  return(output)
}

###############################################################################################################
# Solar Energy Forecast related functions  - computes diurnal for recent one week - used in persistence model I
###############################################################################################################
getLast7DaysSolarPowerByNodalId = function(conn, nodalId, FxDate, SF_capacity, orgId, pssId) {
  
  # Get Last 7 days data from Wind_Eactual
  
  df = getAllDataFromSolarEactualByNodalId(conn, nodalId, FxDate, SF_capacity, orgId, pssId)   #outputs cleaned data
  if(nrow(df)>3){
    #  df$ENERGY = df$POWER/4
    colnames(df) = c("nodalId",  "block_id", "Datatime", "Power", "Energy")
    #df$Energy = df$Power/4
    #########  
    # checking for data availability
    df = getoneweekdataforanydf(df, FxDate)
  }
  return(df)
}

getoneweekdataforanydf = function(df, FxDate) {
  #df$Energy = df$Power/4
  # checking for data availability
  if(as.numeric(difftime(FxDate, max(date(df$Datatime)), units="days")) > 7){
    #message("Data for 7 days is not available.")
    nr=nrow(df)
  }
  if(as.numeric(difftime(FxDate, min(date(df$Datatime)), units="days")) < 7){
    #message("Data for 7 days is not available.")
    nr=nrow(df)
  }
  # if the data is not available for yesterday, then use the most recent 7 days of data
  # adate = as.Date(FxDate) - 2     #yesterday
  # nr = which(adate==date(df$Datatime))[num_blocks]   #yesterday's 96th block
  if (FxDate < today()){
    df = df %>% filter(date(df$Datatime) <= as.Date(FxDate) - 1)
  }
  nr = nrow(df)
  if (is.na(nr)) {
    adate = max(date(df$Datatime)) - 1
    nr = which(adate==date(df$Datatime))[num_blocks]  
  }
  # If recent 672 observations are not available, then it should consider the available data
  if (nrow(df) < num_blocks_diurnal){
    df = df[1:(nr), ]   
    #message("Data for recent 672 observations is not available")
  } else{
    df = df[(nr-(num_blocks_diurnal-1)):(nr), ]   #from yesterday's 96 block to 672 previous observations                               #******************
  }
  return(df)
  
}


get_persistence1_actual_data <- function(orgId,pssId,nodalId,SF_capacity,StartDate,EndDate,num_days,avc_df){
 
  res = get_30_days_actual_data(nodalId,FxDate,SF_capacity,orgId,pssId,num_days,StartDate, EndDate,avc_df)
  
  if(is.data.frame(res) & nrow(res)>0){
    
    res = remove_bad_days(res,nodalId, projType, orgId,pssId,
                          SF_capacity,num_days,StartDate,EndDate,avc_df)
    res = na.omit(res, c("Power"))
    if(!is.data.frame(res) || nrow(res)<96){
      return(data.frame())
    }
    colnames(res) = c("FARM_ID",  "INDEX1",   "BLOCK_ID", "DATA_DATETIME", "POWER" )
    # # --------------------- Defactor the actual by AVC fraction ---------------------
    unique_date_res = unique(date(res$DATA_DATETIME))
    avc_df_res = avc_df
    if(!("FARM_ID" %in% colnames(avc_df_res))){
      avc_df_res = avc_df_res %>% 
        dplyr::mutate(FARM_ID = nodalId) %>%
        dplyr::select (FARM_ID, FX_DATE, BLOCK, AVL_QTM)
    }
    colnames(avc_df_res) = c("FARM_ID",  "FX_DATE",   "BLOCK_ID",    "AVL_QTM" )
    avc_df_res$FX_DATE = as.character(avc_df_res$FX_DATE)
    avc_df_res$FX_DATE = as.Date(avc_df_res$FX_DATE)
    res[, "FX_DATE"] = as.character(res$DATA_DATETIME)
    res[, "FX_DATE"] = as.Date(res$DATA_DATETIME)
    res1 = dplyr::left_join(res, avc_df_res, by = c("FARM_ID", "FX_DATE","BLOCK_ID"))
    res1[is.na(res1$AVL_QTM), "AVL_QTM"] = SF_capacity 
    res1$POWER = ifelse(res1$AVL_QTM/SF_capacity==0, res1$POWER,
                        res1$POWER / (res1$AVL_QTM/SF_capacity) ) 
    res = res1 %>% dplyr::select(-c(FX_DATE , AVL_QTM))
    res$ENERGY = res$POWER/4 
    # # ------------------------------------------------------------
  }else{
    return(data.frame())
  }
  
  names(res) = c("nodalId","index1","block_id","Datatime", "Power", "Energy")
  return(res)
}
