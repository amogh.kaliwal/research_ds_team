import flask
from flask import request, jsonify
import numpy as np
import pandas as pd
import ann_model
import json
from datetime import datetime,date
from flask.json import JSONEncoder

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, date):
            return obj.isoformat()
        else:
            return super(NpEncoder, self).default(obj)

        



class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)
        
app = flask.Flask(__name__)
#app.config["DEBUG"] = True
#app.json_encoder = CustomJSONEncoder
# A route to return the forecast of farm using ann model.
@app.route('/solar/ann', methods=['GET'])
def var_check():
    try:
        if request.args.getlist('FxDate'):
            FxDate = request.args['FxDate']
            
        else:
            return "Error: No FxDate provided. Please specify a FxDate."
        
        if request.args.getlist('sf_capacity'):
            sf_capacity = (request.args['sf_capacity'])
        else:
            return "Error: No sf_capacity provided. Please specify a sf_capacity."
       
        if request.args.getlist('nodalId'):
            nodalId = (request.args['nodalId'])
        else:
            return "Error: No nodalId provided. Please specify a nodalId."
       
        if request.args.getlist('orgId'):
            orgId = (request.args['orgId'])
        else:
            return "Error: No orgId provided. Please specify a orgId."
       
        if request.args.getlist('pssId'):
            pssId = (request.args['pssId'])
        else:
            return "Error: No pssId provided. Please specify a pssId."
        
        if request.args.getlist('StartDate'):
            StartDate = (request.args['StartDate'])
            #StartDate = datetime.datetime.strptime(StartDate, '%Y-%m-%d').date()

        else:
            return "Error: No StartDate provided. Please specify a StartDate."
        
        
        pred = ann_model.sequential_nn_main(FxDate,sf_capacity,nodalId, orgId, pssId, StartDate)

        data=json.dumps(pred.to_dict(orient='records'),cls=NpEncoder)  
      
        #data = [FxDate,sf_capacity,nodalId, orgId, pssId, StartDate]
        
    except Exception as e:
            data = pd.DataFrame()
            data = data.append({'status' : f"False",'Message' : f"{e}"}, 
                        ignore_index = True)  
            data = data.to_json()
    return jsonify(data)

if __name__ == "__main__":
    app.run(debug=True)

#url
#FxDate="2021-04-20"&sf_capacity=22500&nodalId=232&orgId=91&pssId=31&StartDate="2020-12-20"
