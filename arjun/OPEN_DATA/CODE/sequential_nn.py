#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 12:19:00 2021

@author: ktln36
"""
import os
os.getcwd()
os.chdir("/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/CODE")

#################################  import libraries  #################################
import tensorflow as tf
import data_preprocess
import matplotlib.pyplot as plt # for data visualization
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from timeit import default_timer as timer
import pickle
from pathlib import Path
import os.path
from keras.models import model_from_yaml


#FxDate = np.datetime64(date.today()) -21
def sequential_nn_main(FxDate,sf_capacity):
    
            
    ''' Merge the actual power with rad'''
    merged_df = data_preprocess.merge_power_rad()
    
    print("FxDate is ",FxDate) 
    
    ''' Split the data into train and test '''
    train_data, test_data = data_preprocess.divide_train_test(FxDate,merged_df)
    ''' Get the input and output variables in train and test data '''
    X_train, y_train, X_test, y_test = data_preprocess.split_X_Y(train_data, test_data)
    
    print(X_train.shape,y_train.shape, X_test.shape,  y_test.shape)
    
    path = f'/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/TRAINED_MODEL_DAYWISE'
    model_save_time = path+"/"+FxDate+".yaml"
    print("Path for saving trained model",model_save_time)
    print("Check whether model is already saved or not-->",os.path.isfile(model_save_time))
    
    if os.path.isfile(model_save_time)==False:
        print("Saved model is NOT FOUND!!!")

        # determine the number of input features
        n_features = X_train.shape[1]
        
        # define deeper model based on paper
        def deeper_model():
            # create model
            model = Sequential()
            model.add(Dense(n_features, input_dim= n_features, kernel_initializer='normal', activation='relu'))
            model.add(Dense(n_features + 1, kernel_initializer='normal', activation='relu'))
            model.add(Dense((2/3 * n_features), kernel_initializer='normal', activation='relu'))
            model.add(Dense(1, kernel_initializer='normal'))
            # Compile model
            model.compile(loss="mse", optimizer='adam', metrics=[tf.keras.metrics.MeanSquaredError()])
            return model
        
        estimator = KerasRegressor(build_fn=deeper_model)
        # fit the model
        start = timer()
        estimator.fit(X_train, y_train, epochs=20, batch_size=5, verbose= 2)
        end = timer()
        print("Time elapsed in fitting the models is {}".format(end-start))

        #get the summary of the model
        estimator.model.summary()
        #get the weights of the models
        weights = estimator.model.layers[0].get_weights()[0]
        biases = estimator.model.layers[0].get_weights()[1]
        print("weights",weights  )
        print("length of weights",  len(weights))
        print("biases",biases  )
        print("length of biases",  len(biases))
        Path(path).mkdir(parents=True, exist_ok=True)
    
        #Serialize model to yaml
        model_yaml = estimator.model.to_yaml()
        with open(model_save_time, "w") as yaml_file:
            yaml_file.write(model_yaml)
        # serialize weights to HDF5
        estimator.model.save_weights("model.h5")
        print("Saved model to disk")
        
    else:
        # load YAML and create model
        yaml_file = open(model_save_time, 'r')
        loaded_model_yaml = yaml_file.read()
        yaml_file.close()
        estimator = model_from_yaml(loaded_model_yaml)
        # load weights into new model
        estimator.load_weights("model.h5")
        print("Loaded model from disk")
        #get the summary of the model
        estimator.summary()
        #get the weights of the models
        weights = estimator.layers[0].get_weights()[0]
        biases = estimator.layers[0].get_weights()[1]
        print("weights",weights  )
        print("length of weights",  len(weights))
        print("biases",biases  )
        print("length of biases",  len(biases))

     
    
    # make a prediction
    input = X_test
    output = estimator.predict(input)
    y_test["Predicted_power"] = output
    y_test["block_id"] = range(1,97)
    y_test["Predicted_power"] = np.where( (20 < y_test["block_id"] ) & (y_test["block_id"] < 78), y_test["Predicted_power"], 0)

    df = pd.concat([y_test,X_test],axis=1,join="outer",).reset_index()
    start = FxDate + " " + "00:00:00"
    end = FxDate + " " + "23:45:00"
    df["Datetime"] = pd.date_range(start,end,freq="15T")
    df=df[['Power', 'Predicted_power', 'block_id', 'temp', 'dni', 'Datetime']]
    return(df)

FxDate = "2021-05-20"
sf_capacity = 22.5*1000#Sf capacity for gmr is 22.5 MW
y_test =sequential_nn_main(FxDate,sf_capacity)

f = plt.figure()
f.set_figwidth(14)
f.set_figheight(7)
plt.plot(y_test.block_id,y_test.Predicted_power,label = "Predicted_power")
plt.plot(y_test.block_id,y_test.Power,label = "Actual Power")
plt.xlabel('block_id')
# Set the y axis label of the current axis.
plt.ylabel('Power')
# Set a title of the current axes.
plt.title(f"Actual vs Predicted Power Plot of {FxDate}" )
plt.legend()
plt.show()


'''
#################################  GRID SEARCH  #################################
#get list of scoring options
 
sorted(sklearn.metrics.SCORERS.keys())
parameters = {'batch_size': [5,10,15,20],'nb_epoch': [5,10,15,20],'optimizer': ['SGD', 'RMSprop', 'Adagrad','adam']}   
grid_search = GridSearchCV(estimator = model, param_grid = parameters, scoring = 'neg_root_mean_squared_error', cv = 10)
grid_search=grid_search.fit(X_train,y_train)
best_parameters = grid_search.best_params_
best_accuracies= grid_search.best_score_
print(best_parameters)    
print(best_accuracies)  
    
################################ Save mape for  Training purpose#################################

df = pd.DataFrame.from_dict(dict_mape, orient="index")
path = f'/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/SEQUENTIAL/DEEPER/GRID_SEARCHED'
save = path+'/'+'grid_search_based_weights_bias.csv'
df.to_csv(save)
#df = pd.read_csv("formulae_based_weights_bias.csv", index_col=0)

'''

    

