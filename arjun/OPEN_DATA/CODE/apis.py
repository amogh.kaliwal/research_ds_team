import constants
import json
import data_preprocess
import requests
import pandas as pd
import numpy as np
import os
from pathlib import Path
from datetime import datetime
from timeit import default_timer as timer
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 16:37:14 2021

@author: arjun
"""
""" Encoding of input variables to their respective class """

class NpEncoder(json.JSONEncoder):
    # function for assigning class data type
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj) # returning integer
        elif isinstance(obj, np.floating):
            return float(obj) # returning float
        elif isinstance(obj, np.ndarray):
            return obj.tolist() # returning tolist
        else:
            return super(NpEncoder, self).default(obj) # returning default

def Tdiff(data):
    
    if pd.isnull(data.ENTRY_TIME):
        result = constants.hour_const + 1
    else:
        # entry time formating for different weather source
        one_col = datetime.strptime(data.DATA_DATETIME, '%Y-%m-%d %H:%M:%S')
        two_col = datetime.strptime(str(data.ENTRY_TIME), '%Y-%m-%d %H:%M:%S')
        
        # calculate time differnce values in seconds
        result = ((one_col-two_col).total_seconds())

    # return time differnce values in seconds
    return result


def get_sf_capacity(nodalId, orgId, pssId, StartDate, FxDate, headers,projType = "SOLAR"):
    # define api
    sfcapacity_api_farm = "rerffarmparkcapacity"
    sfcapacity_api_pss = "rerffarmparkcapacitypss"
 
    if nodalId == -1:
      api = f"{constants.api_base}/{sfcapacity_api_pss}/"
      payload = json.dumps({
        "orgId": orgId,
        "pssId": pssId,
        "projType":projType
      }, cls=NpEncoder)
    else:   # extract data @ farm level
      api = f"{constants.api_base}/{sfcapacity_api_farm}/"
      payload = json.dumps({
        "farmId": nodalId,
        "projType": projType
      }, cls=NpEncoder)  
    
    # get the data through POST method
    response = requests.request("POST", api, headers = headers, data = payload)
    x = response.json()     # convert data into JSON
    
    # error handling of non-availability of data
    if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':              
        print("No SF_capacity data found in DB for nodalId  ",nodalId)
        pass
    else:
        api_df = pd.DataFrame(x['data'])    # extract required data
        # subset required columns
        api_df = api_df.iloc[0,0]
        print("SF_capacity of  nodalId ",nodalId," is: ",api_df)
    return api_df


def get_avc(nodalId, orgId, pssId, StartDate, FxDate, headers,projType = "SOLAR"):
    # define api
    avc_api_farm = "rerfavc"
    avc_api_pss = "rerfavcpss"
    real_time_avc_farm = "rerfactavc"
 
    if nodalId == -1:
      api = f"{constants.api_base}/{avc_api_pss}/"
      payload = json.dumps({
        "orgId": orgId,
        "pssId": pssId,
        "startDate": StartDate,
        "endDate": FxDate,
        "projType":projType
      }, cls=NpEncoder)
    else:   # extract data @ farm level
      api = f"{constants.api_base}/{avc_api_farm}/"
      #Colnames of non real time AVC 
      #Index(['NODAL_ID', 'FX_DATE', 'BLOCK', 'AVL_QTM', 'IS_MODIFIED'], dtype='object')  
      payload = json.dumps({
            "farmId": nodalId,
            "startDate": StartDate,
            "endDate": FxDate
          }, cls=NpEncoder)
    
    # get the data through POST method
    response = requests.request("POST", api, headers = headers, data = payload)
    x = response.json()     # convert data into JSON
    
    # error handling of non-availability of data
    if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':              
        print("No AVC data found in DB for nodalId  ",nodalId)
        pass
    else:
        api_df = pd.DataFrame(x['data'])    # extract required data
    
    if nodalId == 432:
        api_df = api_df.rename(columns={
                            'NODAL_ID': 'nodalId',
                            'BLOCK': 'block_id',
                            'FX_DATE': 'Date' })
        api_df['ACT_AVL_QTM'] = api_df['AVL_QTM'] - api_df['IS_MODIFIED']
    else:
        api_df = api_df.rename(columns={
                            'NODAL_ID': 'nodalId',
                            'BLOCK': 'block_id',
                            'FX_DATE': 'Date'})
        api_df['ACT_AVL_QTM'] = api_df['AVL_QTM'] - api_df['IS_MODIFIED']
    return api_df
'''  
    elif nodalId == 432:   # extract data @ farm level
      api = f"{constants.api_base}/{real_time_avc_farm}/"
      payload = json.dumps({
        "farmId": nodalId,
        "startDate": StartDate,
        "endDate": FxDate
      }, cls=NpEncoder)
      x = requests.request("POST", api, headers = headers, data = payload).json()
      #Colnames of real time avc for tetratech farms.
      #Index(['NODAL_ID', 'DATA_DATE', 'BLOCK_ID', 'ACT_AVL_QTM'], dtype='object')

      if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
          print("Real time AVC not available for nodalId",nodalId)
          api = f"{constants.api_base}/{avc_api_farm}/"
          payload = json.dumps({
            "farmId": nodalId,
            "startDate": StartDate,
            "endDate": FxDate
            }, cls=NpEncoder)
      else:
          print("Real time AVC is available for nodalId",nodalId)
'''         

""" SOLCAST data extraction """

def solcast_weather(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    solcast_api_farm = "rerfwfxsolcast"
    solcast_api_pss = "rerfwfxsolcastpss"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    final_solcast_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                       'DATA_DATETIME',
                                       'GHI',
                                       'WIND_SPEED',
                                       'DNI',
                                       'DHI',
                                       'TEMPERATURE',
                                       'ZENITH',
                                       'AZIMUTH',
                                       'CLOUD_OPACITY',
                                       'ENTRY_TIME'])
    
    # Loop for extraction solcast data for required dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        # i = "2020-05-01"
        save_path, save_file = data_preprocess.save_solcast_data_path(nodalId, orgId, pssId, i, headers)
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)
            # extract data @ pss level 
            if nodalId == -1:
              solcast_wsapi=f"{constants.api_base}/{solcast_api_pss}/"
              payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": i,
                "endDate": i
              }, cls=NpEncoder)
            else:   # extract data @ farm level
              solcast_wsapi = f"{constants.api_base}/{solcast_api_farm}/"
              payload = json.dumps({
                "farmId": nodalId,
                "startDate": i,
                "endDate": i
              }, cls=NpEncoder)  
            
            # get the data through POST method
            response = requests.request("POST", solcast_wsapi, 
                                        headers = headers, 
                                        data = payload)
            x = response.json()     # convert data into JSON
            
            # error handling of non-availability of data
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                pass
            else:
                solcast_rad = pd.DataFrame(x['data'])    # extract required data
                
                # subset required columns
                solcast_rad = solcast_rad[['BLOCK_ID',
                                           'DATA_DATETIME',
                                           'GHI',
                                           'WIND_SPEED',
                                           'DNI',
                                           'DHI',
                                           'AIR_TEMP',
                                           'ZENITH',
                                           'AZIMUTH',
                                           'CLOUD_OPACITY',
                                           'ENTRY_TIME']]
                
                # rename TEMPERATURE columns
                
                solcast_rad = solcast_rad.rename(columns={
                        'AIR_TEMP': 'TEMPERATURE'})
                
                # rbind data for all required dates
                final_solcast_rad = final_solcast_rad.append(pd.DataFrame(data = 
                                                                      solcast_rad),
                                                         ignore_index=True)
                solcast_rad.to_csv(save_file)
                print("Following csv file is saved: ",save_file)
        
        elif os.path.isfile(save_file)==True:
            solcast_rad = pd.read_csv(save_file)
            # rbind data for all required dates
            final_solcast_rad = final_solcast_rad.append(pd.DataFrame(data = solcast_rad),ignore_index=True)
    '''
    # choose the un-updated data for radiation
    final_solcast_rad["time_diff"] = final_solcast_rad.apply(Tdiff, axis = 1)
    data = final_solcast_rad[final_solcast_rad["time_diff"] >= constants.hour_const]
    data = data.sort_values("time_diff").groupby("DATA_DATETIME",
                           as_index = False).first()
    data.drop(["time_diff"], axis = 'columns', inplace = True)
    
    # generate all the blocks and drop ENTRY_TIME attribute
    df_final = extract_processing(data, 'RADIATION')
    df_final.drop(['ENTRY_TIME'], axis = 'columns', inplace = True)
    '''
    # change the format
    df_final = final_solcast_rad.copy()
    df_final = df_final.astype({'TEMPERATURE': 'float64', 
                                'DNI': 'float64',
                                'DHI': 'float64', 
                                'GHI': 'float64',
                                'ZENITH': 'float64',
                                'AZIMUTH': 'float64', 
                                'WIND_SPEED': 'float64',
                                'BLOCK_ID': 'float64'})
    
    # remove all negatives from data frame
    df_final[['TEMPERATURE','DHI','DNI']] = df_final[['TEMPERATURE','DHI','DNI'
            ]].mask(df_final[['TEMPERATURE','DHI','DNI']] < 0, 0)
    
    return df_final


""" METEOTEST data extraction """

def meteotest_weather(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    meteo_api_farm = "rerfwfxmeteotestcloudmv"
    meteo_api_pss = "rerfwfxmeteotestcloudmvpss"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    final_meteo_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                              'DATA_DATETIME',
                                              'RADIATION', 
                                              'TEMPERATURE',
                                              'ENTRY_TIME'])
    
    # Loop for extraction meteo data for required dates
    for i in seq_dates:
        save_path, save_file = save_data_path(nodalId, orgId, pssId, i, headers,"METEOTEST")
        # print the running date
        print(i)
        # i = "2020-05-01"
        
        # extract data @ pss level
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)

            if nodalId == -1:
              meteo_wsapi=f"{constants.api_base}/{meteo_api_pss}/"
          
              payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": i,
                "endDate": i
              }, cls=NpEncoder)
            else:   # extract data @ farm level
              meteo_wsapi = f"{constants.api_base}/{meteo_api_farm}/"
          
              payload = json.dumps({
                "farmId": nodalId,
                "startDate": i,
                "endDate": i
              }, cls=NpEncoder)
          
        
        # get the data through POST method
            response = requests.request("POST", meteo_wsapi, 
                                    headers = headers, 
                                    data = payload)
        
            x = response.json()     # convert data into JSON
            print(x)
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                pass
        
            else:
                meteo_rad = pd.DataFrame(x['data'])    # extract required data
                #print(len(meteo_rad['DATA_DATETIME'].unique()))
                print(meteo_rad)
                # subset required columns
                meteo_rad = meteo_rad[['BLOCK_ID',
                                   'DATA_DATETIME',
                                   'GLOBAL_RADIATION_HORIZONTAL_W_M',
                                   'AIR_TEMP_CELSIUS',
                                   'ENTRY_TIME']]
            
                # rename RADIATION & TEMPERATURE columns
                meteo_rad = meteo_rad.rename(columns={
                    'GLOBAL_RADIATION_HORIZONTAL_W_M': 'RADIATION'})
                meteo_rad = meteo_rad.rename(columns={
                    'AIR_TEMP_CELSIUS': 'TEMPERATURE'})
            
                # rbind data for all required dates
                final_meteo_rad = final_meteo_rad.append(pd.DataFrame(data = 
                                                                  meteo_rad),
                                                     ignore_index=True)
                meteo_rad["time_diff"] = meteo_rad.apply(Tdiff, axis = 1)

                meteo_rad = meteo_rad[meteo_rad["time_diff"] >= constants.hour_const]
                meteo_rad = meteo_rad.sort_values("time_diff").groupby("DATA_DATETIME",
                           as_index = False).first()
                meteo_rad.drop(["time_diff"], axis = 'columns', inplace = True)
                meteo_rad.to_csv(save_file, index=False)
                print("Following csv file is saved: ",save_file)
                
        elif os.path.isfile(save_file)==True:
            meteo_rad = pd.read_csv(save_file)
            # rbind data for all required dates
            final_meteo_rad = final_meteo_rad.append(pd.DataFrame(data = meteo_rad),ignore_index=True)
        
    
    # change the format
    df_final = final_meteo_rad.copy()
    print(df_final.columns)
    df_final = df_final.astype({'TEMPERATURE': 'float64', 
                                'RADIATION': 'float64',
                                'BLOCK_ID': 'float64'})
    
    '''# remove all negatives from data frame
    df_final[['TEMPERATURE','DHI','DNI']] = df_final[['TEMPERATURE','DHI','DNI'
            ]].mask(df_final[['TEMPERATURE','DHI','DNI']] < 0, 0)'''

        
    return df_final


""" DARKSKY data extraction """

def darksky_weather(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
        # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    final_darksky_data = pd.DataFrame(columns = ['BLOCK_ID',
                                                 'DATA_DATETIME',
                                                 'WIND_SPEED',
                                                 'PRECIP_INTENSITY',
                                                 'PRECIP_PROBABILITY',
                                                 'DEW_PT_TEMP',
                                                 'HUMIDITY',
                                                 'SURFACE_PRESSURE',
                                                 'CLOUD_COVER',
                                                 'ENTRY_TIME'])
    
    # define api
    darksky_api_farm = "rerfwfxdarkskyfx"
    darksky_api_pss = "rerfwfxdarkskyfxpss"
    
    #for i in range(len(seq_dates)-1):
    for i in seq_dates:
        save_path, save_file = save_data_path(nodalId, orgId, pssId, i, headers,"DARKSKY")
        # print the running date
        print('darksky',i)
        
        
        # extract data @ pss level
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)
            # extract data @ pss level
            if nodalId == -1:
                darksky_wsapi = f"{constants.api_base}/{darksky_api_pss}/"
                payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": i,
                "endDate": i,
                "minutes":60
                }, cls=NpEncoder)
            else:   # extract data @ farm level
                darksky_wsapi=f"{constants.api_base}/{darksky_api_farm}/"
                payload = json.dumps({
                "farmId": nodalId,
                "startDate": i,
                "endDate": i,
                "minutes": 60
                }, cls=NpEncoder)
            print(payload)
            print(darksky_wsapi)
            # subset required columns
            response = requests.request("POST", 
                                    darksky_wsapi, 
                                    headers = headers, 
                                    data = payload)
            print(response)
            x = response.json()
            # data = [json.loads(line) for line in open('data.json', 'r')]
        
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                # Create the pandas DataFrame
                data = pd.DataFrame(columns = ['BLOCK_ID',
                                           'DATA_DATETIME',
                                           'WIND_SPEED',
                                           'PRECIP_INTENSITY',
                                           'PRECIP_PROBABILITY',
                                           'DEW_PT_TEMP',
                                           'HUMIDITY',
                                           'SURFACE_PRESSURE',
                                           'CLOUD_COVER',
                                           'ENTRY_TIME'])
                pass
            else:
                darksky_data = pd.DataFrame(x['data'])    # extract required data
            
                # subset required columns
                df = darksky_data[['BLOCK_ID',
                               'DATA_DATETIME',
                               'WIND_SPEED',
                               'PRECIP_INTENSITY',
                               'PRECIP_PROBABILITY',
                               'DEW_PT_TEMP',
                               'HUMIDITY',
                               'SURFACE_PRESSURE',
                               'CLOUD_COVER',
                               'ENTRY_TIME']]
            
                # Get time differnce for subseting un-updated data
                df['ENTRY_TIME'] = df['ENTRY_TIME'].apply(lambda x: 
                    datetime.strptime(x,'%Y-%m-%d %H:%M:%S.%f'))
                df['DATA_DATETIME'] = df['DATA_DATETIME'].apply(lambda x:
                    datetime.strptime(x,'%Y-%m-%d %H:%M:%S'))
                df["time_diff"]=(df.DATA_DATETIME - df.ENTRY_TIME
                  ).astype('timedelta64[s]')
            
                # Choose un-updated darksky data
                data = df[df["time_diff"] >= constants.hour_const]
                data = data.sort_values("time_diff").groupby("DATA_DATETIME",
                                   as_index=False).first()
                data.drop(["time_diff"], axis = 'columns', inplace = True)
                data['DATA_DATETIME'] = data['DATA_DATETIME'].astype(str)
                data['ENTRY_TIME'] = data['ENTRY_TIME'].astype(str)
            
            # rbind data for all required dates
            final_darksky_data = final_darksky_data.append(
                pd.DataFrame(data = data), ignore_index = True)
            data.to_csv(save_file, index=False)
            print("Following csv file is saved: ",save_file)
                
        elif os.path.isfile(save_file)==True:
            data = pd.read_csv(save_file)
            # rbind data for all required dates
            final_darksky_data = final_darksky_data.append(pd.DataFrame(data = data),ignore_index=True)
    return final_darksky_data

""" ACTUAL RADIATION extraction """
'''
def actual_radiation(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    actrad_api_farm = "rerfswact"   
    actrad_api = f"{constants.api_base}/{actrad_api_farm}/"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    act_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                      'DATA_DATETIME',
                                      'RADIATION_ACTUAL'])
    
    # Loop for extraction meteo data for reuired dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        save_path, save_file = save_data_path(nodalId, orgId, pssId, i, headers,"ACTUAL_RADIATION_DATA")
        
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)
            # define input parameters for API
            payload = json.dumps({
                "farmId": nodalId,
                "startDate": i
            }, cls=NpEncoder)
            
            # query the data from API
            response = requests.request("POST", 
                                        actrad_api, 
                                        headers = headers, 
                                        data = payload)
            x = response.json()
            
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                continue
            else:
                df = pd.DataFrame(x['data'])
            
                # subset required columns
                df = df[['BLOCK_ID', 'DATATIME', 'RAD_ACT']]
                
                # rename the date time column
                df = df.rename(columns={'RAD_ACT': 'RADIATION_ACTUAL',
                                        'DATATIME': 'DATA_DATETIME'})
                
                # subseting data for day and bind it final data frame
                df = subset_day_data(df, i)
                
                # generate all the blocks and drop ENTRY_TIME attribute
                df_final = extract_processing(df, 'RADIATION_ACTUAL')
                df_final.to_csv(save_file,index=False)
                print("Following csv file is saved as it is not found in local: ",save_file)
                # rbind data for all required dates
                act_rad = act_rad.append(pd.DataFrame(data = df_final), 
                                         ignore_index = True)
        elif os.path.isfile(save_file)==True:
            print("The following actual radiation data is found in local. Hence reading from local-->",save_file)
            df = pd.read_csv(save_file)
            # rbind data for all required dates
            act_rad = act_rad.append(pd.DataFrame(data = df),ignore_index=True)
        
    return act_rad
'''
""" ACTUAL POWER extraction """

def actual_power(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    actpower_api_farm = "rerfsdiurnal"
    actpower_api_pss = "rerfsdiurnalpss"
 
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, end = FxDate).strftime('%Y-%m-%d')
        
    # Create the pandas DataFrame
    final_actpower = pd.DataFrame(columns = ['FARM_ID' ,
                                           'BLOCK_ID' ,
                                           'WEEK' ,
                                           'HOUR' ,
                                           'MINUTES' ,
                                           'DATATIME' ,
                                           'POWER' ])
    
    # Loop for extraction solcast data for required dates
    for i in seq_dates:
        # print the running date
        print(i)
        # i = "2020-05-01"
        save_path, save_file = save_data_path(nodalId, orgId, pssId, i, headers,"ACTUAL_POWER_DATA")
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)
            print("The following actual data is not found. Hence reading from DB and saving it-->",save_file)            
            # extract data @ pss level 
            if nodalId == -1:
              solcast_wsapi=f"{constants.api_base}/{actpower_api_pss}/"
              payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": i,
                "endDate": i
              }, cls=NpEncoder)
            else:   # extract data @ farm level
              solcast_wsapi = f"{constants.api_base}/{actpower_api_farm}/"
              payload = json.dumps({
                "farmId": nodalId,
                "startDate": i,
                "endDate": i
              }, cls=NpEncoder)  
            
            # get the data through POST method
            response = requests.request("POST", solcast_wsapi, 
                                        headers = headers, 
                                        data = payload)
            x = response.json()     # convert data into JSON
            
            # error handling of non-availability of data
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                empty_df = pd.DataFrame(columns = ['FARM_ID' ,
                                           'BLOCK_ID' ,
                                           'WEEK' ,
                                           'HOUR' ,
                                           'MINUTES' ,
                                           'DATATIME' ,
                                           'POWER' ])
                #Save an empty dataframe if no act data is not available
                empty_df.to_csv(save_file,index=False)
                print("No actpower data found in DB. Following empty csv file is saved: ",save_file)
                continue
            else:
                actpower = pd.DataFrame(x['data'])    # extract required data
                
                # subset required columns
                actpower = actpower[['FARM_ID' ,
                                    'BLOCK_ID' ,
                                    'WEEK' ,
                                    'HOUR' ,
                                    'MINUTES' ,
                                    'DATATIME' ,
                                    'POWER' 
                                    ]]
            
                # rbind data for all required dates
                final_actpower = final_actpower.append(pd.DataFrame(data = 
                                                                      actpower),
                                                         ignore_index=True)
                actpower.to_csv(save_file,index=False)
                print("Following csv file is saved: ",save_file)
        
        elif os.path.isfile(save_file)==True:
            print("The following actual data is found in local. Hence reading from local-->",save_file)
            actpower = pd.read_csv(save_file)
            # rbind data for all required dates
            final_actpower = final_actpower.append(pd.DataFrame(data = actpower),ignore_index=True)

    # change the format
    df_final = final_actpower.copy()
    df_final = df_final.astype({'FARM_ID': 'float64', 
                                'BLOCK_ID': 'float64',
                                'WEEK': 'float64', 
                                'HOUR': 'float64',
                                'MINUTES': 'float64',
                                'POWER': 'float64'
                                })
    
    # remove all negatives from data frame
    df_final[['POWER']] = df_final[['POWER']].mask(df_final[['POWER']] < 0, 0)
    
    return df_final

def save_data_path( nodalId, orgId, pssId, FxDate, headers, data):
    datetime64Obj = np.datetime64(FxDate) 
    path = r"/home/arjun/research_ds_team/arjun/OPEN_DATA/DATA"
    save_path = path+"/" + data+"/"+str(orgId)+"/"+str(pssId)+"/"+str(nodalId)
    save_path = save_path + "/"+str(datetime64Obj.astype(object).year)+"/"+str(datetime64Obj.astype(object).month)+"/"+str(datetime64Obj.astype(object).day)
    save_data = save_path+"/"+str(FxDate)+"_"+str(orgId)+"_"+str(pssId)+"_"+str(nodalId)+"_"+data+".csv"
    #print("Path for saving trained model",save_solcast)
    #print("Check whether csv is already saved or not-->",os.path.isfile(save_solcast)) 
    return(save_path,save_data)
