import os
os.chdir('/home/apiuser/research_ds_team/arjun/OPEN_DATA/CODE')
#os.chdir('C:/research_ds_team/arjun/OPEN_DATA/CODE')
import tensorflow as tf, data_preprocess, numpy as np,pandas as pd, apis, constants, os.path
import data_preprocess
from timeit import default_timer as timer
from datetime import datetime
from pathlib import Path
headers = constants.headers
#######################################################
def sequential_nn_main(FxDate, wfxid, nodalId, orgId, pssId, fxtype, blkid, cmfx, ndays, gran):
    FxDate = FxDate.replace("\\","").replace('"',"")   
    StartDate = str(np.datetime64(FxDate) - 365*2)
    StartDate = StartDate.replace("\\","").replace('"',"")
    sf_capacity = apis.get_sf_capacity(nodalId, orgId, pssId, StartDate, FxDate, headers,projType = "SOLAR")
    model_save_time = data_preprocess.save_trained_model_path(nodalId, orgId, pssId, StartDate, FxDate)   
    if (os.path.isdir(model_save_time)==True) :
        if (len(os.listdir(model_save_time)) > 0):
            X_train, y_train, X_test, y_test, input, avc = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, 
                                                                                           FxDate, sf_capacity,saved=1)
            estimator = tf.keras.models.load_model(model_save_time)
        elif (len(os.listdir(model_save_time)) == 0):
            Path(model_save_time).mkdir(parents=True, exist_ok=True)
            #saved=0
            X_train, y_train, X_test, y_test, input, avc = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, 
                                                                                           FxDate, sf_capacity,saved=0)
            estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)             
    else: 
        Path(model_save_time).mkdir(parents=True, exist_ok=True)
        X_train, y_train, X_test, y_test, input, avc = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, 
                                                                                       FxDate, sf_capacity,saved=0)
        estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)     

    weights,biases,pred = estimator.layers[0].get_weights()[0],estimator.layers[0].get_weights()[1],estimator.predict(input)
    output = data_preprocess.post_process(FxDate, pred, input, avc, sf_capacity, fxtype, blkid)
    #print('FxType is ',fxtype)
    #print('blkid is ',blkid)
    #if fxtype ==1:
     #   output = output[output['block_id']>= blkid]
      #  print("output1 is ",output.head())
    print("output2 is ",output)
    return(output)

###############      TEST AREA       ####################

'''
FxDate = "2021-10-01"
FxDate = FxDate
wfxid = 24
nodalId=432
orgId=70
pssId=178
fxtype=1
blkid=25
cmfx=0
ndays=1
gran=15
output = sequential_nn_main(FxDate, wfxid, nodalId, orgId, pssId, fxtype, blkid, cmfx, ndays, gran)
data_preprocess.plot_output(FxDate = FxDate, output = output)
'''