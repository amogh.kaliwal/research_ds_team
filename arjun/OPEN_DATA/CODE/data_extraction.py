# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 11:34:43 2021

@author: arjun
"""

import constants
import requests
import pandas as pd
import numpy as np
import json
import os
from datetime import datetime
headers = constants.headers

''' Preprocess the solcast data '''
def data_preprocsess():
    os.chdir("/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/DATA")
    df = pd.read_csv("23.91_71.22_Solcast_PT15M.csv")
    df  = df[['PeriodStart','AirTemp','Dhi', 'Dni', 'Ghi','WindSpeed10m', 'Zenith']]
    df.columns = ['Datatime', 'temp', 'dhi','dni','ghi','ws','zenith']
    df.zenith[df.zenith > 90] = 90
    
    #sun height = 90-zenith angle
    df["sun_height"] = 90 - df.zenith
    
    # Reflected radiation calculation.
    df["refl_rad"] = df.ghi*0.2*((1-np.cos(np.pi*20/180))/2)
    
    #Global Horizontal (GHI) = Direct Normal (DNI) X cos(θ) + Diffuse Horizontal (DHI)
    #df["ghi_cal"] = df.dni*np.cos(np.pi*df.zenith/180)+df.dhi
     
    df["Datatime"] = df['Datatime'].str.split("Z",1,expand = True)[0]
    #tt=df['DATATIME'].str.split(" ",1,expand = True)
    df["date"] = df['Datatime'].str.split("T",1,expand = True)[0]
    df["hms"] = df['Datatime'].str.split("T",1,expand = True)[1]
    df["Datatime"] = df["date"] + " " + df["hms"] 
    df = df[['Datatime', 'temp', 'dhi', 'dni', 'ws', 'zenith', 'sun_height','refl_rad']]
    return df

def merge_power_rad():
    ################################################
    ''' Load data '''
    os.chdir("/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/DATA")

    act_power = pd.read_csv('gmr_actual_power.csv')
    solcast = data_preprocsess()
    merged_df = act_power.merge(solcast, how='inner', on='Datatime')
    return(merged_df)
    
'''
from data_extraction import actual_power
#Get actual power data
act_power = actual_power( headers)
#preprocess the dataset
data = data_preprocsess(df)
data.to_csv('solcast_data.csv', index=False, sep=',')
#write the actual power dataframe to csv file 
act_power.to_csv('actual_power.csv', index=False, sep=',')
'''
...
'''
df = read_csv("df_aggregated.csv")

df.info()
df.isnull().sum()
df.describe()
'''
""" Encoding of input variables to their respective class """

class NpEncoder(json.JSONEncoder):
    # function for assigning class data type
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj) # returning integer
        elif isinstance(obj, np.floating):
            return float(obj) # returning float
        elif isinstance(obj, np.ndarray):
            return obj.tolist() # returning tolist
        else:
            return super(NpEncoder, self).default(obj) # returning default

""" Calculate time differnce in seconds between datetime and entrytime """

def Tdiff(data):
    
    if pd.isnull(data.ENTRY_TIME):
        result = constants.hour_const + 1
    else:
        # entry time formating for different weather source
        one_col = datetime.strptime(data.Datatime, '%Y-%m-%d %H:%M:%S')
        two_col = datetime.strptime(str(data.ENTRY_TIME), '%Y-%m-%d %H:%M:%S')
        
        # calculate time differnce values in seconds
        result = ((one_col-two_col).total_seconds())

    # return time differnce values in seconds
    return result

""" Calculate block from datetime column """

def get_block_from_time(now):
    
    a = now.hour    # extract hour from date
    b = now.minute   # extract minute from date
    block = 4*a+1 + b//15    # calculate block
    
    return block

""" generate block and time at 15 mins """

def missing_blocks_processing(data):
    
    # generate n-days with 15 mins granularity
    max_date = data['Datatime'].max().split()[0]
    min_date = data['Datatime'].min().split()[0]
    datelist = pd.date_range(start = pd.Timestamp(min_date + 'T00'), 
                             end = pd.Timestamp(max_date + 'T23:45'), 
                             freq = '15T')
    df = pd.DataFrame({"Datatime": datelist})
    df['BLOCK_ID'] = df['Datatime'].apply(lambda x: get_block_from_time(x))
    df['Datatime'] = df['Datatime'].astype(str)
    
    return df

""" extracted data processing steps """

def extract_processing(data, replace_column):
    
    # generate all the blocks and drop ENTRY_TIME attribute
    df = missing_blocks_processing(data)
    df = df.rename(columns={'BLOCK_ID': 'block_id'})
    # prepare a data frame with all values
    merged_data = pd.merge(df, data, 
                           on = ['Datatime', 'block_id'], 
                           how = 'left')
    
    '''# replace all the values to zero above 75th and 22nd block
    merged_data.loc[merged_data.block_id >= 75, replace_column] = 0
    merged_data.loc[merged_data.block_id <= 22, replace_column] = 0
    '''
    
    # to interpolate the missing values
    df_final = merged_data.interpolate(method = 'piecewise_polynomial', 
                                       limit_direction = 'backward', 
                                       order = 5)
    
    return df_final

""" function to subset data for a day """

def subset_day_data(data, chosen_date):
    
    # subseting data for day and bind it final data frame
    tt = data['Datatime'].str.split(" ", n = 1, expand = True)
    data = data[tt[0] == chosen_date]
    data = data.reset_index(drop = True)
    
    return data

""" ACTUAL POWER extraction """

def actual_power( headers):
        
    # define api
    actpower_api_farm = "rerfsdiurnal"
    actpower_api_pss = "rerfsdiurnalpss"
    StartDate ="2018-12-31"
    FxDate = "2021-06-05"
    nodalId = 232
    pssId = 91
    orgId = 31
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    act_power = pd.DataFrame(columns = ['block_id',
                                        'Datatime'])
    
    # Loop for extraction meteo data for reuired dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        
        # extract data @ pss level
        if nodalId == -1:
            actpower_api = f"{constants.api_base}/{actpower_api_pss}/"
            payload = json.dumps({
                "orgId": orgId,
                "pssId": pssId,
                "startDate": i
                }, cls=NpEncoder)
        else:   # extract data @ farm level
            actpower_api = f"{constants.api_base}/{actpower_api_farm}/"
            payload = json.dumps({
                "farmId": nodalId,
                "startDate": i
                }, cls=NpEncoder)
        
        # query the data from API    
        response = requests.request("POST", 
                                    actpower_api, 
                                    headers = headers, 
                                    data = payload)
        x = response.json()
        
        if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
            continue
        else:
            df = pd.DataFrame(x['data'])
        
            # rename the date time column
            df = df.rename(columns={'DATATIME': 'Datatime'})
            df = df.rename(columns={'POWER': 'Power'})
            df = df.rename(columns={'BLOCK_ID': 'block_id'})
            # subset required columns
            df = df[['block_id', 'Datatime', 'Power']]
            
            # subseting data for day and bind it final data frame
            df1 = subset_day_data(df, i)
                    
            if df1.shape[0]>0:
                # generate all the blocks and drop ENTRY_TIME attribute
                df_final = extract_processing(df1, 'Power')
                        
                # rbind data for all required dates
                act_power = act_power.append(pd.DataFrame(data = df_final), 
                                             ignore_index = True)
        
    return act_power    

def divide_train_test(nodalId, orgId, pssId, StartDate, 
                      FxDate, headers):
    
    # data = merge_meteo_darksky
    
    # retrive combined data
    data = combine_data(nodalId, orgId, pssId, StartDate, 
                        FxDate, headers)
    
    # create a date column in data
    data['DATA_DATE'] = data['DATA_DATETIME'].str.split(" ", n = 1, 
        expand = True)[0]
    
    # choose FxDate data as test and rest as train
    test_data = data[(data['DATA_DATE'] == FxDate)]
    train_data = data[(data['DATA_DATE'] < FxDate)]
    
    # return data for data processing
    return train_data, test_data


