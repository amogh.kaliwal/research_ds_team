
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  9 15:49:52 2021

@author: arjun
"""

import os
os.chdir(r"C:\KREATE\PYTHON_TASKS\arjun\OPEN_DATA\CODE")

#################################  import libraries  #################################
import tensorflow as tf
import data_preprocess
import matplotlib.pyplot as plt # for data visualization
import numpy as np
import pandas as pd
from timeit import default_timer as timer
from pathlib import Path
import apis
import constants
import os.path
from keras.models import model_from_yaml
headers = constants.headers

###############      TEST AREA       ####################

########################################################
#FxDate = np.datetime64(date.today()) -21
def sequential_nn_main(FxDate,nodalId, orgId, pssId, StartDate):
    FxDate = FxDate.replace("\\","").replace('"',"")    
    StartDate = StartDate.replace("\\","").replace('"',"")
    print("FxDate is : ", FxDate)
    print("StartDate is : ", StartDate)
    sf_capacity = apis.get_sf_capacity(nodalId, orgId, pssId, StartDate, FxDate, headers,projType = "SOLAR")
    #print(X_train.shape,y_train.shape, X_test.shape,  y_test.shape,input.shape)
    model_save_time = data_preprocess.save_trained_model_path(nodalId, orgId, pssId, StartDate, FxDate)
    print("Path for saving trained model",model_save_time)
    print("Check whether model is already saved or not-->",os.path.isdir(model_save_time))
    
    if (os.path.isdir(model_save_time)==True) :
        if (len(os.listdir(model_save_time)) > 0):
            ''' Get the input and output variables in train and test data '''
            X_train, y_train, X_test, y_test, input, avc = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, 
                                                                                           FxDate, sf_capacity,saved=1)
            estimator = tf.keras.models.load_model(model_save_time)
            print("Loaded model from disk")
                        
        elif (len(os.listdir(model_save_time)) == 0):
            print("Saved model is NOT FOUND!!!")
            ''' Get the input and output variables in train and test data '''
            Path(model_save_time).mkdir(parents=True, exist_ok=True)
            X_train, y_train, X_test, y_test, input, avc = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, 
                                                                                           FxDate, sf_capacity,saved=0)
            estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)        
             
    else: 
        print("Saved model is NOT FOUND!!!")
        Path(model_save_time).mkdir(parents=True, exist_ok=True)
        ''' Get the input and output variables in train and test data '''
        X_train, y_train, X_test, y_test, input, avc = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, 
                                                                                       FxDate, sf_capacity,saved=1)
        estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)  

    #get the summary of the model
    estimator.summary()
    #get the weights of the models
    weights = estimator.layers[0].get_weights()[0]
    biases = estimator.layers[0].get_weights()[1]
    print("weights",weights  )
    print("length of weights",  len(weights))
    print("biases",biases  )
    print("length of biases",  len(biases))
    # make a prediction
    pred = estimator.predict(input)
    output = data_preprocess.post_process(FxDate,pred,input,avc,sf_capacity)
    return(output)


FxDate = "2021-09-30"
StartDate = "2019-09-25"
EndDate =  FxDate
'''
#gmr
nodalId=232
orgId=91
pssId=31
sf_capacity = 22.5*1000#Sf capacity for gmr is 22.5 MW
'''
#Gorontalo
nodalId=432
orgId=70
pssId=178


'''
#likupang
nodalId=424
orgId=70
pssId=172


#Sf capacity for gmr is 22.5 MW
GMR: good till-2021-06-18 
to be removed : 2021-06-19 to 2021-09-20
good from 2021-09-21 as per new scheduler.

LIKUPANG:
    good from 2021-09-21 as per new scheduler.
    
    
GORONTALO:
    good from 2021-09-21 as per new scheduler.
    

'''

start = timer()
output =sequential_nn_main(FxDate,nodalId, orgId, pssId, StartDate)
end = timer()
print("Time elapsed in model execution is {}".format(end-start))

f = plt.figure()
f.set_figwidth(14)
f.set_figheight(7)
plt.plot(output.block_id,output.Predicted_power,label = "Predicted_power")
plt.xlabel('block_id')
# Set the y axis label of the current axis.
plt.ylabel('Power')
# Set a title of the current axes.
plt.title(f"Predicted Power Plot of {FxDate}" )
plt.legend()
plt.show()
=======
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  9 15:49:52 2021

@author: arjun
"""

import os
os.getcwd()
os.chdir("/home/arjun/projects/api")

#################################  import libraries  #################################
import tensorflow as tf
import data_preprocess
import matplotlib.pyplot as plt # for data visualization
import numpy as np
import pandas as pd
from timeit import default_timer as timer
from pathlib import Path
import os.path
from keras.models import model_from_yaml

###############      TEST AREA       ####################
'''
FxDate = "2021-04-04"
StartDate = "2021-02-20"
#gmr
nodalId=232
orgId=91
pssId=31
sf_capacity = 22.5*1000#Sf capacity for gmr is 22.5 MW
'''

########################################################
#FxDate = np.datetime64(date.today()) -21
def sequential_nn_main(FxDate,sf_capacity,nodalId, orgId, pssId, StartDate):
    FxDate = FxDate.replace("\\","").replace('"',"")    
    StartDate = StartDate.replace("\\","").replace('"',"")
    print("FxDate is : ", FxDate)
    print("StartDate is : ", StartDate)
 
    ''' Merge the actual power with rad'''
    merged_df,input = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, FxDate)
       
    ''' Split the data into train and test '''
    train_data, test_data = data_preprocess.divide_train_test(FxDate,merged_df)
    ''' Get the input and output variables in train and test data '''
    X_train, y_train, X_test, y_test = data_preprocess.split_X_Y(train_data, test_data)
    
    print(X_train.shape,y_train.shape, X_test.shape,  y_test.shape,input.shape)
    model_save_time = data_preprocess.save_trained_model_path(nodalId, orgId, pssId, StartDate, FxDate)
    print("Path for saving trained model",model_save_time)
    print("Check whether model is already saved or not-->",os.path.isdir(model_save_time))
    
    if (os.path.isdir(model_save_time)==True) :
        if (len(os.listdir(model_save_time)) > 0):
            '''
            # load YAML and create model
            yaml_file = open(model_save_time, 'r')
            loaded_model_yaml = yaml_file.read()
            yaml_file.close()
            estimator = model_from_yaml(loaded_model_yaml)
            # load weights into new model
            estimator.load_weights("model.h5")
            '''        
            estimator = tf.keras.models.load_model(model_save_time)
            print("Loaded model from disk")
            #get the summary of the model
            estimator.summary()
            #get the weights of the models
            weights = estimator.layers[0].get_weights()[0]
            biases = estimator.layers[0].get_weights()[1]
            print("weights",weights  )
            print("length of weights",  len(weights))
            print("biases",biases  )
            print("length of biases",  len(biases))
            
        elif (len(os.listdir(model_save_time)) == 0):
            print("Saved model is NOT FOUND!!!")
            Path(model_save_time).mkdir(parents=True, exist_ok=True)
            estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)
             
    else: 
        print("Saved model is NOT FOUND!!!")
        Path(model_save_time).mkdir(parents=True, exist_ok=True)
        estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)


    # make a prediction
    pred = estimator.predict(input)
    flat_pred = [item for sublist in pred for item in sublist]
    data = {"Predicted_power": flat_pred}
    output = pd.DataFrame(data )
    output["block_id"] = range(1,97)
    output["Predicted_power"] = np.where( (output["block_id"] >20) & (output["block_id"] < 78), output["Predicted_power"], 0)
    output["Predicted_power"] = np.where( (output["Predicted_power"] <0) , 0 , output["Predicted_power"])
    start = FxDate + " " + "00:00:00"
    end = FxDate + " " + "23:45:00"
    output["Datetime"] = pd.date_range(start,end,freq="15T")
    output = pd.concat([output.reset_index(drop=True), input[["dni","temp"]].reset_index(drop=True)], axis=1)
    return(output)

'''
start = timer()
output =sequential_nn_main(FxDate,sf_capacity,nodalId, orgId, pssId, StartDate)
end = timer()
print("Time elapsed in model execution is {}".format(end-start))

f = plt.figure()
f.set_figwidth(14)
f.set_figheight(7)
plt.plot(output.block_id,output.Predicted_power,label = "Predicted_power")
plt.xlabel('block_id')
# Set the y axis label of the current axis.
plt.ylabel('Power')
# Set a title of the current axes.
plt.title(f"Predicted Power Plot of {FxDate}" )
plt.legend()
plt.show()
'''
