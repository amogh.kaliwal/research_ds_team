import os
os.chdir('/home/apiuser/research_ds_team/arjun/OPEN_DATA/CODE')
import flask
from flask import request, jsonify
import numpy as np
import pandas as pd
import test_file
import json
from datetime import datetime,date
from flask.json import JSONEncoder
from collections import OrderedDict

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, date):
            return obj.isoformat()
        else:
            return super(NpEncoder, self).default(obj)

    

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)
        
app = flask.Flask(__name__)
#app.config["DEBUG"] = True
#app.json_encoder = CustomJSONEncoder
# A route to return the forecast of farm using ann model.
@app.route('/solar/ann', methods=['GET'])
def var_check():
    try:
        if request.args.getlist('FxDate'):
            FxDate = request.args['FxDate']
            FxDate = FxDate.replace("\\","").replace('"',"")   
            
        else:
            return "Error: No FxDate provided. Please specify a FxDate."
        '''
        if request.args.getlist('sf_capacity'):
            sf_capacity = (request.args['sf_capacity'])
        else:
            return "Error: No sf_capacity provided. Please specify a sf_capacity."
        if request.args.getlist('StartDate'):
            StartDate = (request.args['StartDate'])
            #StartDate = datetime.datetime.strptime(StartDate, '%Y-%m-%d').date()
        else:
            return "Error: No StartDate provided. Please specify a StartDate."
       '''
       
     
        if request.args.getlist('wfxid'):
            wfxid = (request.args['wfxid'])
        else:
            return "Error: No wfxid provided. Please specify a wfxid."
        
        if request.args.getlist('nodalId'):
            nodalId = (request.args['nodalId'])
        else:
            return "Error: No nodalId provided. Please specify a nodalId."
       
        if request.args.getlist('orgId'):
            orgId = (request.args['orgId'])
        else:
            return "Error: No orgId provided. Please specify a orgId."
       
        if request.args.getlist('pssId'):
            pssId = (request.args['pssId'])
        else:
            return "Error: No pssId provided. Please specify a pssId."
        
        if request.args.getlist('fxtype'):
            fxtype = (request.args['fxtype'])
        else:
            return "Error: No fxtype provided. Please specify a fxtype."
        
        if request.args.getlist('blkid'):
            blkid = (request.args['blkid'])
        else:
            return "Error: No blkid provided. Please specify a blkid."
        
        if request.args.getlist('cmfx'):
            cmfx = (request.args['cmfx'])
        else:
            return "Error: No cmfx provided. Please specify a cmfx."
        
        if request.args.getlist('ndays'):
            ndays = (request.args['ndays'])
        else:
            return "Error: No ndays provided. Please specify a ndays."
        
        if request.args.getlist('gran'):
            gran = (request.args['gran'])
        else:
            return "Error: No gran provided. Please specify a gran."
        
        pred = test_file.sequential_nn_main(FxDate,wfxid,nodalId,orgId, pssId,fxtype,blkid,cmfx,ndays,gran)
        fxdata =  pred[['Datetime', 'block_id', 'Predicted_power', 'dni', 'temp']].rename(columns={
                            'Datetime': 'ddt',
                            'block_id': 'bid',
                            'Predicted_power': 'fx',
                            'dni': 'rad'}).to_dict(orient='records')
               
        fxdata = { "sts": "TRUE", "msg": "Forecast generated successfully","rmt": "12","wfxid": "24",
                  "fbid": blkid,'fid':nodalId,'pssid':pssId,'orgid':orgId,"fxdt": FxDate,"fxdata":fxdata
                  }
        data = json.dumps(fxdata,cls=NpEncoder)  

    except Exception as e:
            data = pd.DataFrame()
            data = data.append({'status' : f"False",'Message' : f"{e}"}, 
                        ignore_index = True)  
            data = data.to_json()
    #return jsonify(data)
    #data_content = json.loads(input_data.decode('utf-8'), object_pairs_hook=OrderedDict)

    return json.loads(data.replace("\'", '"'), object_pairs_hook=OrderedDict)

if __name__ == "__main__":
    app.run(host = '0.0.0.0',port = 5550)


#Test api
#http://65.1.92.166:5550/solar/ann?FxDate=2021-09-30&wfxid=24&nodalId=432&orgId=70&pssId=178&fxtype=1&blkid=25&cmfx=0&ndays=1&gran=15
