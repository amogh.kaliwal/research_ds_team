#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  1 04:23:15 2021

@author: ktln36
"""
import os
os.getcwd()
os.chdir("C:/KREATE/PYTHON_TASKS/arjun/OPEN_DATA/CODE")

import tensorflow as tf
import data_preprocess
import pandas as pd # for data analysis
import matplotlib.pyplot as plt # for data visualization
import numpy as np
from datetime import datetime
from datetime import date
from sklearn.metrics import make_scorer
from sklearn.metrics import explained_variance_score, r2_score, mean_squared_error
from sklearn.model_selection import GridSearchCV
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
import sklearn
from sklearn import metrics
from keras.layers.core import Dense, Activation
from keras. models import Model
from sklearn.model_selection import GridSearchCV
from timeit import default_timer as timer
from pathlib import Path
import os.path
from keras.models import model_from_yaml

#FxDate = np.datetime64(date.today()) -21
#StartDate =  np.datetime64(FxDate) -21
sf_capacity = 22.5*1000#Sf capacity for gmr is 22.5 MW
FxDate = "2021-10-08"
StartDate = "2021-02-20"

#Gorontalo
nodalId=432
orgId=70
pssId=178
sf_capacity = 10.75*1000#Sf capacity for gmr is 22.5 MW
sf_capacity = apis.get_sf_capacity(nodalId, orgId, pssId, StartDate, FxDate, headers,projType = "SOLAR")

'''
#gmr
nodalId=232
orgId=91
pssId=31
sf_capacity = 22.5*1000#Sf capacity for gmr is 22.5 MW

#likupang
nodalId=424
orgId=70
pssId=172
sf_capacity = 16.27*1000#Sf capacity for gmr is 22.5 MW
'''
''' Merge the actual power with rad'''

days = pd.date_range('2021-09-21', '2021-09-28')
print("days are ",days) 
dict_mape = {}
for i in range(0,len(days)):

    FxDate = days[i].strftime('%Y-%m-%d')
    print("FxDate is ",FxDate) 
    StartDate =  np.datetime64(FxDate) -(365*2)
    print("StartDate is ",StartDate) 
    merged_df,input = data_preprocess.merge_power_rad(nodalId, orgId, pssId, StartDate, FxDate)
   
    ''' Split the data into train and test '''
    train_data, test_data = data_preprocess.divide_train_test(FxDate,merged_df)
    
     #Remove the wrongly ingested data from scheduler of GMR
    if nodalId == 232:
        train_data=train_data[(train_data['date']<'2021-06-18')|(train_data['date']>'2021-09-20')]
        test_data=test_data[(test_data['date']<'2021-06-18')|(test_data['date']>'2021-09-20')]
        
    ''' Get the input and output variables in train and test data '''
    X_train, y_train, X_test, y_test = data_preprocess.split_X_Y(train_data, test_data)
    print("No. of rows of train data",len(X_train))
    print("No. of rows of train data",len(X_test))
    
    print(X_train.shape,y_train.shape, X_test.shape,  y_test.shape,input.shape)
    model_save_time = data_preprocess.save_trained_model_path(nodalId, orgId, pssId, StartDate, FxDate)
    print("Path for saving trained model",model_save_time)
    print("Check whether model is already saved or not-->",os.path.isdir(model_save_time))
   
    if(os.path.isdir(model_save_time)==True) :
        if (len(os.listdir(model_save_time)) > 0):
            '''
            # load YAML and create model
            yaml_file = open(model_save_time, 'r')
            loaded_model_yaml = yaml_file.read()
            yaml_file.close()
            estimator = model_from_yaml(loaded_model_yaml)
            # load weights into new model
            estimator.load_weights("model.h5")
            '''            
            estimator = tf.keras.models.load_model(model_save_time)
            print("Loaded model from disk")
                       
        elif (len(os.listdir(model_save_time)) == 0):
            print("Saved model is NOT FOUND!!!")
            Path(model_save_time).mkdir(parents=True, exist_ok=True)
            estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)
             
    else: 
        print("Saved model is NOT FOUND!!!")
        Path(model_save_time).mkdir(parents=True, exist_ok=True)
        estimator = data_preprocess.train_model_save(X_train,y_train,model_save_time)

    #get the summary of the model
    estimator.summary()
    #get the weights of the models
    weights = estimator.layers[0].get_weights()[0]
    biases = estimator.layers[0].get_weights()[1]
    print("weights",weights  )
    print("length of weights",  len(weights))
    print("biases",biases  )
    print("length of biases",  len(biases))
    # make a prediction
    pred = estimator.predict(input)
    flat_pred = [item for sublist in pred for item in sublist]
    data = {"Predicted_power": flat_pred}
    output = pd.DataFrame(data )
    output["block_id"] = range(1,97)
    output["Predicted_power"] = np.where( (output["block_id"] >20) & (output["block_id"] < 78), output["Predicted_power"], 0)
    output["Predicted_power"] = np.where( (output["Predicted_power"] <0) , 0 , output["Predicted_power"])

    start = FxDate + " " + "00:00:00"
    end = FxDate + " " + "23:45:00"
    output["Datetime"] = pd.date_range(start,end,freq="15T")
    output = pd.concat([output.reset_index(drop=True), input[["dni","temp"]].reset_index(drop=True)], axis=1)
    
    comb = pd.concat([output.reset_index(drop=True), y_test[["Power"]].reset_index(drop=True)], axis=1)
    comb["rel_dev"] = np.where( comb["Power"] >0, (abs(comb["Predicted_power"]  - comb["Power"] )*100)/sf_capacity , 0)

    import statistics as st
    mape_calc = round(st.mean(comb["rel_dev"]),2)
    #mape = round(mean_absolute_percentage_error(comb["Predicted_power"], comb["Power"])*100,2)
    print("Manual Mape is : ",mape_calc)
    #print("Automated Mape is : ",mape)
    entry={"weights":weights,"biases":biases,'mape_calc':mape_calc}
    dict_mape[FxDate]=entry  
    
    f = plt.figure()
    f.set_figwidth(14)
    f.set_figheight(7)
    plt.plot(comb.block_id,comb.Predicted_power,label = "Predicted_power")
    plt.plot(comb.block_id,comb.Power,label = "Actual Power")
    plt.xlabel('block_id')
    # Set the y axis label of the current axis.
    plt.ylabel('Power')
    # Set a title of the current axes.
    plt.title(f"FxDate is: {FxDate}, MAPE is: {mape_calc} , GRID SEARCHED")
    plt.legend()
    #plt.show()
    
    
    from pathlib import Path
    path = f'C:/KREATE/PYTHON_TASKS/arjun/OPEN_DATA/GRID_SEARCHED/PLOTS/LIVE_DATA/{str(nodalId)}'
    Path(path).mkdir(parents=True, exist_ok=True)
    #plt.savefig(f'/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/SEQUENTIAL/DEEPER/FORMULAE_BASED/{FxDate}.png')
    plt.savefig(path+'/'+'%s.png'%FxDate)

    #plt.savefig(f'/home/ktln36/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/SEQUENTIAL/DEEPER/RANDOM/{FxDate}.png')
    #plt.show()
    del mape_calc,train_data, test_data, output, X_train, y_train, X_test, y_test,estimator,input,FxDate,path,entry,f
    '''
    #################################  GRID SEARCH  #################################
     #get list of scoring options
 
    sorted(sklearn.metrics.SCORERS.keys())
    parameters = {'batch_size': [5,10,15,20],'nb_epoch': [5,10,15,20],'optimizer': ['SGD', 'RMSprop', 'Adagrad','adam']}   
    grid_search = GridSearchCV(estimator = model, param_grid = parameters, scoring = 'neg_root_mean_squared_error', cv = 10)
    grid_search=grid_search.fit(X_train,y_train)
    best_parameters = grid_search.best_params_
    best_accuracies= grid_search.best_score_
    print(best_parameters)    
    print(best_accuracies)  
    
    ################################ Save mape for  Training purpose#################################
 '''
df = pd.DataFrame.from_dict(dict_mape, orient="index")
path = f'C:/KREATE/PYTHON_TASKS/arjun/OPEN_DATA/GRID_SEARCHED/WEIGHTS_BIASES/LIVE_DATA'
Path(path).mkdir(parents=True, exist_ok=True)
save = path+'/'+'grid_search_based_weights_bias.csv'
df.to_csv(save)
#df = pd.read_csv("formulae_based_weights_bias.csv", index_col=0)

  
