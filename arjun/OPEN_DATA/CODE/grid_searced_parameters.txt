#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 12:15:53 2021

@author: ktln36
"""

print(best_parameters)    
    {'batch_size': 5, 'nb_epoch': 20, 'optimizer': 'adam'}
    
    print(best_accuracies)    
    -1751.65963305263
   
   

    #summarize results
    print("Best: %f using %s" % (grid_search.best_score_, grid_search.best_params_))

    grid_search.best_score_, grid_search.best_params_))
Best: -1751.659633 using {'batch_size': 5, 'nb_epoch': 20, 'optimizer': 'adam'}
      
   
   for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
-2387.312824 (1737.068336) with: {'batch_size': 5, 'nb_epoch': 5, 'optimizer': 'SGD'}
-2846.774105 (2093.618464) with: {'batch_size': 5, 'nb_epoch': 5, 'optimizer': 'RMSprop'}
-2685.283968 (1827.631016) with: {'batch_size': 5, 'nb_epoch': 5, 'optimizer': 'Adagrad'}
-2329.701104 (1528.215500) with: {'batch_size': 5, 'nb_epoch': 5, 'optimizer': 'adam'}
-2319.214437 (1479.188942) with: {'batch_size': 5, 'nb_epoch': 10, 'optimizer': 'SGD'}
-2406.166512 (1724.967292) with: {'batch_size': 5, 'nb_epoch': 10, 'optimizer': 'RMSprop'}
-2331.117687 (1733.389400) with: {'batch_size': 5, 'nb_epoch': 10, 'optimizer': 'Adagrad'}
-1798.593962 (429.907891) with: {'batch_size': 5, 'nb_epoch': 10, 'optimizer': 'adam'}
-1799.924376 (382.235194) with: {'batch_size': 5, 'nb_epoch': 15, 'optimizer': 'SGD'}
-1833.262862 (352.347033) with: {'batch_size': 5, 'nb_epoch': 15, 'optimizer': 'RMSprop'}
-1770.572856 (394.012972) with: {'batch_size': 5, 'nb_epoch': 15, 'optimizer': 'Adagrad'}
-2239.578610 (1268.980244) with: {'batch_size': 5, 'nb_epoch': 15, 'optimizer': 'adam'}
-1792.971582 (402.738240) with: {'batch_size': 5, 'nb_epoch': 20, 'optimizer': 'SGD'}
-1779.864398 (412.084788) with: {'batch_size': 5, 'nb_epoch': 20, 'optimizer': 'RMSprop'}
-1808.956560 (408.502563) with: {'batch_size': 5, 'nb_epoch': 20, 'optimizer': 'Adagrad'}
-1751.659633 (396.491624) with: {'batch_size': 5, 'nb_epoch': 20, 'optimizer': 'adam'}
-2345.619300 (1734.889403) with: {'batch_size': 10, 'nb_epoch': 5, 'optimizer': 'SGD'}
-1824.014824 (386.858144) with: {'batch_size': 10, 'nb_epoch': 5, 'optimizer': 'RMSprop'}
-2779.202034 (1899.098899) with: {'batch_size': 10, 'nb_epoch': 5, 'optimizer': 'Adagrad'}
-2348.883769 (1543.141160) with: {'batch_size': 10, 'nb_epoch': 5, 'optimizer': 'adam'}
-2176.528946 (1322.319735) with: {'batch_size': 10, 'nb_epoch': 10, 'optimizer': 'SGD'}
-2678.418504 (1666.917762) with: {'batch_size': 10, 'nb_epoch': 10, 'optimizer': 'RMSprop'}
-2903.232434 (1992.876231) with: {'batch_size': 10, 'nb_epoch': 10, 'optimizer': 'Adagrad'}
-1805.789966 (419.994964) with: {'batch_size': 10, 'nb_epoch': 10, 'optimizer': 'adam'}
-1796.530878 (401.707784) with: {'batch_size': 10, 'nb_epoch': 15, 'optimizer': 'SGD'}
-2887.014676 (2006.421862) with: {'batch_size': 10, 'nb_epoch': 15, 'optimizer': 'RMSprop'}
-2308.310775 (1817.624708) with: {'batch_size': 10, 'nb_epoch': 15, 'optimizer': 'Adagrad'}
-1776.175197 (400.157838) with: {'batch_size': 10, 'nb_epoch': 15, 'optimizer': 'adam'}
-1839.526155 (410.419957) with: {'batch_size': 10, 'nb_epoch': 20, 'optimizer': 'SGD'}
-1813.712458 (393.270363) with: {'batch_size': 10, 'nb_epoch': 20, 'optimizer': 'RMSprop'}
-2361.631754 (1791.960149) with: {'batch_size': 10, 'nb_epoch': 20, 'optimizer': 'Adagrad'}
-2360.350378 (1725.426477) with: {'batch_size': 10, 'nb_epoch': 20, 'optimizer': 'adam'}
-2844.913986 (2008.950701) with: {'batch_size': 15, 'nb_epoch': 5, 'optimizer': 'SGD'}
-1841.854564 (403.715982) with: {'batch_size': 15, 'nb_epoch': 5, 'optimizer': 'RMSprop'}
-2422.157021 (1731.483087) with: {'batch_size': 15, 'nb_epoch': 5, 'optimizer': 'Adagrad'}
-1820.808605 (429.525798) with: {'batch_size': 15, 'nb_epoch': 5, 'optimizer': 'adam'}
-2371.718922 (1735.426703) with: {'batch_size': 15, 'nb_epoch': 10, 'optimizer': 'SGD'}
-2253.075834 (1279.197510) with: {'batch_size': 15, 'nb_epoch': 10, 'optimizer': 'RMSprop'}
-3242.888300 (2069.208652) with: {'batch_size': 15, 'nb_epoch': 10, 'optimizer': 'Adagrad'}
-2785.036573 (2011.084515) with: {'batch_size': 15, 'nb_epoch': 10, 'optimizer': 'adam'}
-2367.806123 (1473.991189) with: {'batch_size': 15, 'nb_epoch': 15, 'optimizer': 'SGD'}
-2608.926177 (1710.268004) with: {'batch_size': 15, 'nb_epoch': 15, 'optimizer': 'RMSprop'}
-3989.833208 (2471.981164) with: {'batch_size': 15, 'nb_epoch': 15, 'optimizer': 'Adagrad'}
-2365.327407 (1466.570870) with: {'batch_size': 15, 'nb_epoch': 15, 'optimizer': 'adam'}
-1837.667330 (414.812168) with: {'batch_size': 15, 'nb_epoch': 20, 'optimizer': 'SGD'}
-1832.620580 (382.143887) with: {'batch_size': 15, 'nb_epoch': 20, 'optimizer': 'RMSprop'}
-1821.972937 (394.229953) with: {'batch_size': 15, 'nb_epoch': 20, 'optimizer': 'Adagrad'}
-2298.629625 (1267.775759) with: {'batch_size': 15, 'nb_epoch': 20, 'optimizer': 'adam'}
-2273.627860 (1466.499102) with: {'batch_size': 20, 'nb_epoch': 5, 'optimizer': 'SGD'}
-2323.522244 (1815.870398) with: {'batch_size': 20, 'nb_epoch': 5, 'optimizer': 'RMSprop'}
-1839.956098 (420.789925) with: {'batch_size': 20, 'nb_epoch': 5, 'optimizer': 'Adagrad'}
-1844.931166 (397.208692) with: {'batch_size': 20, 'nb_epoch': 5, 'optimizer': 'adam'}
-1877.006265 (384.921973) with: {'batch_size': 20, 'nb_epoch': 10, 'optimizer': 'SGD'}
-2364.076494 (1473.460956) with: {'batch_size': 20, 'nb_epoch': 10, 'optimizer': 'RMSprop'}
-2394.447542 (1460.787979) with: {'batch_size': 20, 'nb_epoch': 10, 'optimizer': 'Adagrad'}
-2374.580442 (1602.582691) with: {'batch_size': 20, 'nb_epoch': 10, 'optimizer': 'adam'}
-2957.544630 (2117.080528) with: {'batch_size': 20, 'nb_epoch': 15, 'optimizer': 'SGD'}
-1845.552917 (415.831056) with: {'batch_size': 20, 'nb_epoch': 15, 'optimizer': 'RMSprop'}
-2381.228283 (1469.794561) with: {'batch_size': 20, 'nb_epoch': 15, 'optimizer': 'Adagrad'}
-1843.912354 (407.744970) with: {'batch_size': 20, 'nb_epoch': 15, 'optimizer': 'adam'}
-1859.536493 (383.443702) with: {'batch_size': 20, 'nb_epoch': 20, 'optimizer': 'SGD'}
-2950.837000 (2344.993874) with: {'batch_size': 20, 'nb_epoch': 20, 'optimizer': 'RMSprop'}
-2891.142936 (1977.813112) with: {'batch_size': 20, 'nb_epoch': 20, 'optimizer': 'Adagrad'}
-2380.985139 (1532.306743) with: {'batch_size': 20, 'nb_epoch': 20, 'optimizer': 'adam'}
   
   
   
      