#!/usr/bin/env python
# coding: utf-8

# In[14]:

import os
os.chdir( r"F:/PYTHON_TASKS/CLOUD NOWCASTING/shell_dataset/dataset")
import pandas as pd
import numpy as np
import constants
import matplotlib.pyplot as plt
import json
import requests
from pathlib import Path
import os.path
from timeit import default_timer as timer
headers = constants.headers
from sklearn.metrics import mean_squared_error


""" Encoding of input variables to their respective class """

class NpEncoder(json.JSONEncoder):
    # function for assigning class data type
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj) # returning integer
        elif isinstance(obj, np.floating):
            return float(obj) # returning float
        elif isinstance(obj, np.ndarray):
            return obj.tolist() # returning tolist
        else:
            return super(NpEncoder, self).default(obj) # returning default


def save_data_path(nodalId, orgId, pssId, FxDate, headers, data):
    datetime64Obj = np.datetime64(FxDate) 
    path = f'F:/PYTHON_TASKS/BEST_DAYS_SELECTION/dataset/CLIENT_DATA'
    save_path = path+"/" + data+"/"+str(orgId)+"/"+str(pssId)+"/"+str(nodalId)
    save_path = save_path + "/"+str(datetime64Obj.astype(object).year)+"/"+str(datetime64Obj.astype(object).month)+"/"+str(datetime64Obj.astype(object).day)
    save_data = save_path+"/"+str(FxDate)+"_"+str(orgId)+"_"+str(pssId)+"_"+str(nodalId)+"_"+data+".csv"
    #print("Path for saving trained model",save_solcast)
    #print("Check whether csv is already saved or not-->",os.path.isfile(save_solcast)) 
    return(save_path,save_data)


# In[15]:

nodalId =157
orgId = 24
pssId = 2
StartDate = "2019-09-01" 
FxDate = "2021-09-31"
os.getcwd()


# In[16]:




def actual_radiation(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    actrad_api_farm = "rerfswact"   
    actrad_api = f"{constants.api_base}/{actrad_api_farm}/"
    
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate, 
                              end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    act_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                      'DATA_DATETIME',
                                      'RADIATION_ACTUAL'])
    
    # Loop for extraction meteo data for reuired dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        save_path, save_file = save_data_path(nodalId, orgId, pssId, i, headers,"ACTUAL_RADIATION_DATA")
        
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)
            # define input parameters for API
            payload = json.dumps({
                "farmId": nodalId,
                "startDate": i
            }, cls=NpEncoder)
            
            # query the data from API
            response = requests.request("POST", 
                                        actrad_api, 
                                        headers = headers, 
                                        data = payload)
            x = response.json()
            
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                empty_df = pd.DataFrame(columns = ['BLOCK_ID', 'DATATIME', 'RAD_ACT'])
                #Save an empty dataframe if no act data is not available
                empty_df.to_csv(save_file,index=False)
                print("No act rad data found in DB. Following empty csv file is saved: ",save_file)
                continue

            else:
                df = pd.DataFrame(x['data'])
            
                # subset required columns
                df = df[['BLOCK_ID', 'DATATIME', 'RAD_ACT']]
                
                # rename the date time column
                df = df.rename(columns={'RAD_ACT': 'RADIATION_ACTUAL',
                                        'DATATIME': 'DATA_DATETIME'})
                df["date"] = df['DATA_DATETIME'].str.split(" ",1,expand = True)[0]
                
                # subseting data for day and bind it final data frame
                df = df[df['date']==i]
                
                # generate all the blocks and drop ENTRY_TIME attribute
                #df_final = extract_processing(df, 'RADIATION_ACTUAL')
                df.to_csv(save_file,index=False)
                print("Following csv file is saved as it is not found in local: ",save_file)
                # rbind data for all required dates
                act_rad = act_rad.append(pd.DataFrame(data = df),ignore_index = True)
        elif os.path.isfile(save_file)==True:
            print("The following actual radiation data is found in local. Hence reading from local-->",save_file)
            df = pd.read_csv(save_file)
            # rbind data for all required dates
            act_rad = act_rad.append(pd.DataFrame(data = df),ignore_index=True)
        
    return act_rad


#act_df = actual_radiation(nodalId, orgId, pssId, StartDate, FxDate, headers)

# In[8]:


def save_actrad_data_path(nodalId, orgId, pssId, FxDate, headers):
    datetime64Obj = np.datetime64(FxDate) 
    path = f'F:/PYTHON_TASKS/CLOUD NOWCASTING/shell_dataset/dataset/CLIENT_DATA/ACT_RADIATION'
    save_path = path+"/" +"/"+str(orgId)+"/"+str(pssId)+"/"+str(nodalId)
    save_path = save_path + "/"+str(datetime64Obj.astype(object).year)+"/"+str(datetime64Obj.astype(object).month)+"/"+str(datetime64Obj.astype(object).day)
    save_actrad = save_path+"/"+str(FxDate)+"_"+str(orgId)+"_"+str(pssId)+"_"+str(nodalId)+"_actrad_data"+".csv"
    #print("Path for saving trained model",save_solcast)
    #print("Check whether csv is already saved or not-->",os.path.isfile(save_solcast)) 
    return(save_path,save_actrad)


os.getcwd()
def main_function():
    train = actual_radiation(nodalId, orgId, pssId, StartDate, FxDate, headers)
    train1 = train.copy()
    train1['RADIATION_ACTUAL'] = np.where(train1['RADIATION_ACTUAL']<0,0,train1['RADIATION_ACTUAL'])
    train1['RADIATION_ACTUAL'].isnull().sum()
    train1.dropna(axis=0, how='any',inplace = True)
    #train1 = train1[train1['RADIATION_ACTUAL']==0]
    train1.shape
    train1['month'] = [int(i.split('-')[1]) for i in train1['date']]
    train1.month.unique()
    train1 = train1[train1['month'].isin([8,9])]
    train_set1 = train1[train1['date']<"2021-09-20"]
    test_set1 =  train1[train1['date']>="2021-09-20"]
    date = test_set1.date.unique()
    
    
    all_rmse = pd.DataFrame()
    
    for j in date:
        test_FxDate = test_set1[test_set1['date']==j]
        test_FxDate = test_FxDate[test_FxDate['RADIATION_ACTUAL']>0]
        start = timer()
        combine_fx = pd.DataFrame()
        fxbid = [37,43,49,55,61,67, 73]
        for x in fxbid:
            train_final = pd.DataFrame()
            lst = [int(i) for i in range((x-10),(x-4))]
            boolean_series = test_FxDate['BLOCK_ID'].isin(lst)
            test = test_FxDate[boolean_series]
            if test.shape[0]<1:
                print("x or fxbid is ", x)
                print("next is executed")
                
                continue
            X_train,y_train  = train_set1,train_set1.loc[:,['RADIATION_ACTUAL']]
            X_test, y_test = test,test.loc[:,['RADIATION_ACTUAL']]
            X_test1, y_test1 = X_test.copy(), y_test.copy()
            X_test.reset_index(drop=True, inplace=True)
            y_test.reset_index(drop=True, inplace=True)
            #Sitting at the 11th blockid and forecasting from the next 1 hr so FxBlockId is 11 + 4 = 15th block id.
            #Fxblockid = test.iloc[15,:]['BLOCK_ID']
            test.shape
            #rmse = pd.DataFrame({'DATE':[j],'FxBlockId':[Fxblockid]})
            blocks= 6
            for k in reversed(range(blocks)):
                print("k is ",k)
                #Following is the logic for simulating the real time availbility of actual data and forecasting of rad/power.
                #Sitting at the 11th available non-zero block we are calculating the forecasted radiation from the first
                # 8 available non-zero block excluding the 2 blocks(9th and 10th blocks) as they is usually a lag of two 
                # blocks in the availability of actual data from client side.
                if k<= test.shape[0]:
                    summ = pd.DataFrame({'diff':abs(X_train['RADIATION_ACTUAL'] - X_test['RADIATION_ACTUAL'].iloc[-k])})
                    ind = summ[summ['diff']==summ['diff'].min()].index.astype(int)
                    print("min is ",summ['diff'].min())
                    
                    ft = pd.DataFrame({"RADIATION_ACTUAL":[0 for i in range(16)]})
                    ft["RADIATION_ACTUAL"]=ft["RADIATION_ACTUAL"].astype(float)
                    for l in ind:
                        train = y_train.loc[l:,:].head(16)
                        train.reset_index(drop=True, inplace=True)
                        ft['RADIATION_ACTUAL'] = ft['RADIATION_ACTUAL']+train['RADIATION_ACTUAL']
                        ft
                    train = ft['RADIATION_ACTUAL']/len(ind)
                    train = pd.DataFrame(train)
                    #print(i)
                    #print('original 10 rows of y_train',train['Total Cloud Cover [%]'][0:11])
                    #print('one',train['Total Cloud Cover [%]'][0:i])
                if k > 0:
                    train['RADIATION_ACTUAL'] =train['RADIATION_ACTUAL'].shift(k)
                    #print(train['Total Cloud Cover [%]'][0:i])
                    train['RADIATION_ACTUAL'] = train['RADIATION_ACTUAL'].replace(np.nan,0)
                train_final = pd.concat([train_final, train], axis=0)   
            #del k
            trf = train_final
    
            trf['block_id']=range(1,(trf.shape[0]+1))
            print("trf 1 is ", trf)
            trf['num']=[i for i in range(1,(train.shape[0]+1))]*blocks
            print("trf 2 is ", trf)
    
            '''
            Now add the above stocks recursively in sorted order. I.e. starting with the deafault order, keep the first ten rows. 
            Then add the next ten rows to the starting ten rows from the 2nd row. Add the third
            '''
            a=[]
            for i in range(1,17):
                #print(i)
                b = trf[trf['num']==i]
                #print(b)
                c =np.sum(b[['RADIATION_ACTUAL']])/b.shape[0]
                a.append(c)
            del i
            prediction = pd.DataFrame(a)
            prediction.columns = ["RADIATION_FORECASTED"]
            prediction = prediction.iloc[10:,:]
            
            
            lst_fx = [int(i) for i in range((x),(x+6))]
            bool_series = test_FxDate['BLOCK_ID'].isin( lst_fx )
            combine_df = test_FxDate[test_FxDate['BLOCK_ID'].isin( lst_fx )]
            combine_df.reset_index(drop=True, inplace=True)
            prediction.reset_index(drop=True, inplace=True)
            
            combine_df= pd.concat([combine_df, prediction], axis=1) 
            combine_df.dropna(inplace = True)
             
               
            #squared :bool, default=True : If True returns MSE value, if False returns RMSE value.
           # RMSE_SKLEARN = mean_squared_error(combine_df['RADIATION_FORECASTED'], combine_df['RADIATION_ACTUAL'], squared=False)
           #rmse_df = pd.DataFrame(rmse_df)
            #file = combine_df.date.unique()[0]
           
            end = timer()
           
            print("combine_df",combine_df)
            print(j)
            print("Time elapsed in this iteration is {}".format(end-start))
            #rmse = rmse.merge(rmse_df,right_on =['DATE', 'FxBlockId'],left_on = ['DATE', 'FxBlockId'] ,how = 'inner')
            #all_rmse = pd.concat([all_rmse, rmse], axis = 0)
            combine_fx = pd.concat([combine_fx, combine_df], axis = 0)
            
                 
        f = plt.figure()
        f.set_figwidth(14)
        f.set_figheight(7)
        ax = f.add_subplot(111)
        ax.tick_params(axis='x', colors='red')
        ax.tick_params(axis='y', colors='black')
        plt.plot(combine_fx.BLOCK_ID, combine_fx['RADIATION_FORECASTED'], color = 'orange',label = 'Predicted Radiation')
        plt.plot(combine_fx.BLOCK_ID, combine_fx['RADIATION_ACTUAL'], color = 'blue',label = 'Actual Radiation')
        plt.legend(loc = 'upper left')
        plt.xlabel("BLOCK_ID",color = 'C0')
        plt.ylabel("RADIATION(W/M2)",color = 'C2')
        file = combine_fx.date.unique()[0].split('-')
        file = file[0]+'_'+file[1]+'_'+file[2]
        plt.title(f"FxDate is: {file} ",color = 'C12')
        
        # save the figure
        model_save_time = f'F:/PYTHON_TASKS/BEST_DAYS_SELECTION/dataset/CLIENT_DATA/OUTPUT_PLOTS/SEPTEMBER-OCTOBER' 
        
        Path(model_save_time).mkdir(parents=True, exist_ok=True)
        plt.savefig(model_save_time+'/'+'%s.png'%file,dpi = 400)
        plt.show()
        
        path = f'F:/PYTHON_TASKS/BEST_DAYS_SELECTION/dataset/CLIENT_DATA/OUTPUT_CSV/SEPTEMBER-OCTOBER' 
        Path(path).mkdir(parents=True, exist_ok=True)
        save = path+  "/"+str(j) + '_'+"All BLOCKS Forecast.csv"
        combine_fx.to_csv(save)
        

main_function()
'''
path = f'F:/PYTHON_TASKS/BEST_DAYS_SELECTION/dataset/CLIENT_DATA/OUTPUT_CSV'
save = path+'/'+"All BLOCKS comparison" + "_"+'rmse.csv'
df = pd.read_csv(save)

d = df.dropna()
path = f'F:/PYTHON_TASKS/BEST_DAYS_SELECTION/dataset/CLIENT_DATA/OUTPUT_CSV'
Path(path).mkdir(parents=True, exist_ok=True)
save = path+'/'+"Final All BLOCKS comparison" + "_"+'rmse.csv'
d.to_csv(save)
'''



def data_sent_to_client(nodalId, orgId, pssId, StartDate, FxDate, headers):
    
    # define api
    if nodalId == -1:
        actrad_api_farm = "rerfclientfxschdetailsdsmdtpss"   
        actrad_api = f"{constants.api_base}/{actrad_api_farm}/"
    else:
        actrad_api_farm = "rerfclientfxschdetailsdsmdt"   
        actrad_api = f"{constants.api_base}/{actrad_api_farm}/"
        
    # Create seq of date for data extraction
    seq_dates = pd.date_range(start = StartDate,end = FxDate).strftime('%Y-%m-%d')
    
    # Create the pandas DataFrame
    act_rad = pd.DataFrame(columns = ['BLOCK_ID',
                                      'DATA_DATETIME',
                                      'RADIATION_ACTUAL'])
    
    # Loop for extraction meteo data for reuired dates
    for i in seq_dates:
        
        # print the running date
        print(i)
        save_path, save_file = save_data_path(nodalId, orgId, pssId, i, headers,"DATA_SENT_TO_CLIENT")
        
        if os.path.isfile(save_file)==False:
            Path(save_path).mkdir(parents=True, exist_ok=True)
            # define input parameters for API
            if nodalId == -1:
                payload = json.dumps({
                "orgId": orgId,
                "pssId":pssId,
                "startDate": i
                }, cls=NpEncoder)
            else:
                payload = json.dumps({
                "farmId": nodalId,
                "startDate": i,
                "endDate":i
                }, cls=NpEncoder)
            
            
            # query the data from API
            response = requests.request("POST", 
                                        actrad_api, 
                                        headers = headers, 
                                        data = payload)
            x = response.json()
            
            if x['type'] == 'ERROR' or x['type'] == 'ERR' or x['type'] == 'error':
                empty_df = pd.DataFrame(columns = ['NODAL_ID', 'FX_DATE', 'BLOCK_ID', 'REV_NO', 'R_MODEL_TYPE','SOURCE_ID', 'POWER_FX', 'DSM_CHARGES'])
                #Save an empty dataframe if no act data is not available
                empty_df.to_csv(save_file,index=False)
                print("No act rad data found in DB. Following empty csv file is saved: ",save_file)
                continue

            else:
                df = pd.DataFrame(x['data'])
            
                # subset required columns
                df = df[['BLOCK_ID', 'DATATIME', 'RAD_ACT']]
                
                # rename the date time column
                df = df.rename(columns={'RAD_ACT': 'RADIATION_ACTUAL',
                                        'DATATIME': 'DATA_DATETIME'})
                df["date"] = df['DATA_DATETIME'].str.split(" ",1,expand = True)[0]
                
                # subseting data for day and bind it final data frame
                df = df[df['date']==i]
                
                # generate all the blocks and drop ENTRY_TIME attribute
                #df_final = extract_processing(df, 'RADIATION_ACTUAL')
                df.to_csv(save_file,index=False)
                print("Following csv file is saved as it is not found in local: ",save_file)
                # rbind data for all required dates
                act_rad = act_rad.append(pd.DataFrame(data = df),ignore_index = True)
        elif os.path.isfile(save_file)==True:
            print("The following actual radiation data is found in local. Hence reading from local-->",save_file)
            df = pd.read_csv(save_file)
            # rbind data for all required dates
            act_rad = act_rad.append(pd.DataFrame(data = df),ignore_index=True)
        
    return act_rad

