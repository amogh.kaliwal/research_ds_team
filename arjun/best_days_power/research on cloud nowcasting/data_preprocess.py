#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 28 11:54:26 2021

@author: ktln36
"""
import tensorflow as tf
import pandas as pd
import numpy as np
import constants
import apis
import json
import requests
from pathlib import Path
import os.path
from timeit import default_timer as timer
from keras.models import Sequential
from keras.layers import Dense
headers = constants.headers

""" Encoding of input variables to their respective class """

class NpEncoder(json.JSONEncoder):
    # function for assigning class data type
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj) # returning integer
        elif isinstance(obj, np.floating):
            return float(obj) # returning float
        elif isinstance(obj, np.ndarray):
            return obj.tolist() # returning tolist
        else:
            return super(NpEncoder, self).default(obj) # returning default



def save_solcast_data_path(nodalId, orgId, pssId, FxDate, headers):
    datetime64Obj = np.datetime64(FxDate) 
    path = f'C:/KREATE/PYTHON_TASKS/arjun/OPEN_DATA/DATA/WEATHER_DATA'
    save_path = path+"/" + "SOLCAST"+"/"+str(orgId)+"/"+str(pssId)+"/"+str(nodalId)
    save_path = save_path + "/"+str(datetime64Obj.astype(object).year)+"/"+str(datetime64Obj.astype(object).month)+"/"+str(datetime64Obj.astype(object).day)
    save_solcast = save_path+"/"+str(FxDate)+"_"+str(orgId)+"_"+str(pssId)+"_"+str(nodalId)+"_solcast"+".csv"
    #print("Path for saving trained model",save_solcast)
    #print("Check whether csv is already saved or not-->",os.path.isfile(save_solcast)) 
    return(save_path,save_solcast)



def save_actpower_data_path(nodalId, orgId, pssId, FxDate, headers):
    datetime64Obj = np.datetime64(FxDate) 
    path = f'C:/KREATE/PYTHON_TASKS/arjun/OPEN_DATA/DATA/CLIENT_DATA'
    save_path = path+"/" + "_DATA"+"/"+str(orgId)+"/"+str(pssId)+"/"+str(nodalId)
    save_path = save_path + "/"+str(datetime64Obj.astype(object).year)+"/"+str(datetime64Obj.astype(object).month)+"/"+str(datetime64Obj.astype(object).day)
    save_actpower = save_path+"/"+str(FxDate)+"_"+str(orgId)+"_"+str(pssId)+"_"+str(nodalId)+"_actpower_data"+".csv"
    #print("Path for saving trained model",save_solcast)
    #print("Check whether csv is already saved or not-->",os.path.isfile(save_solcast)) 
    return(save_path,save_actpower)
                
''' Preprocess the solcast data '''
def data_fe(nodalId, orgId, pssId, StartDate, FxDate):
    #df1 = pd.read_csv(constants.base_path+"/DATA/23.91_71.22_Solcast_PT15M.csv")
    #df1  = df1[['PeriodStart','AirTemp','Dhi', 'Dni', 'Ghi','WindSpeed10m', 'Zenith']]
    #df1.columns = ['Datatime', 'temp', 'dhi','dni','ghi','ws','zenith']
    df = apis.solcast_weather(nodalId, orgId, pssId, StartDate, FxDate, headers)
    df["date"] = df['DATA_DATETIME'].str.split(" ",1,expand = True)[0]
    df = df[['BLOCK_ID', 'DATA_DATETIME', 'WIND_SPEED', 'GHI','DNI', 'DHI','TEMPERATURE', 'ZENITH']]
    df.columns = ['block_id', 'Datatime', 'ws', 'ghi','dni', 'dhi','temp', 'zenith']
    df.zenith[df.zenith > 90] = 90
    #sun height = 90-zenith angle
    df["sun_height"] = 90 - df.zenith 
    # Reflected radiation calculation.
    df["refl_rad"] = df.ghi*0.2*((1-np.cos(np.pi*20/180))/2)
    #Global Horizontal (GHI) = Direct Normal (DNI) X cos(θ) + Diffuse Horizontal (DHI)
    #df["ghi_cal"] = df.dni*np.cos(np.pi*df.zenith/180)+df.dhi
    #df["Datatime"] = df['Datatime'].str.split("Z",1,expand = True)[0]
    #tt=df['DATATIME'].str.split(" ",1,expand = True)
    df["date"] = df['Datatime'].str.split(" ",1,expand = True)[0]
    #df["hms"] = df['Datatime'].str.split("T",1,expand = True)[1]
    #df["Datatime"] = df["date"] + " " + df["hms"] 
    df["Datatime"]= pd.to_datetime(df["Datatime"])
    #time_change = pd.Timedelta(hours=5, minutes=30)
    #df["Datatime"] = df["Datatime"] + time_change
    df = df[['block_id','Datatime', 'temp', 'dhi', 'dni', 'refl_rad','ws','sun_height','date']]
    return df

def merge_power_rad(nodalId, orgId, pssId, StartDate, FxDate, sf_capacity,saved):
    ################################################
    ''' Load data '''
    if saved == 0:
        start = timer()
        avc = apis.get_avc(nodalId, orgId, pssId, StartDate, FxDate, headers,projType = "SOLAR")
        end = timer()
        print("Time elapsed in getting avc is {}".format(end-start))
        #act_power = pd.read_csv(constants.base_path+'/DATA/gmr_actual_power.csv')
        start = timer()
        act_power = apis.actual_power(nodalId, orgId, pssId, StartDate, FxDate, headers)
        end = timer()
        print("Time elapsed in getting Actual power data is {}".format(end-start))
        act_power = act_power.rename(columns={
                            'FARM_ID': 'nodalId',
                            'BLOCK_ID': 'block_id',
                            'DATATIME': 'Datatime',
                            'POWER': 'Power'})
        act_power["Datatime"] = pd.to_datetime(act_power["Datatime"], errors='coerce')
       
        act_power['Date'] = act_power["Datatime"].dt.strftime('%Y-%m-%d')
        
        #Defactoring of actual power for training
        act_power = act_power.merge(avc, how='inner', on=['Date','block_id'])
        act_power['Power'] = act_power['Power']*(sf_capacity/act_power['ACT_AVL_QTM'])  
        avc = avc[avc['Date']==FxDate]
        
        start = timer()
        solcast = data_fe(nodalId, orgId, pssId, StartDate, FxDate)
        end = timer()
        print("Time elapsed in getting Solcast data is {}".format(end-start))
        solcast = solcast[['block_id', 'Datatime', 'temp', 'dhi', 'dni', 'refl_rad', 'sun_height', 'date']]
        start = timer()
        solcast = data_fe(nodalId, orgId, pssId, StartDate, FxDate)
        end = timer()
        print("Time elapsed in getting Solcast data is {}".format(end-start))
        solcast = solcast[['block_id', 'Datatime', 'temp', 'dhi', 'dni', 'refl_rad', 'sun_height', 'date']]
        act_power["Datatime"]= pd.to_datetime(act_power["Datatime"])
        merged_df = act_power.merge(solcast, how='inner', on=['Datatime','block_id'])
        merged_df = merged_df.dropna()
        
        #Remove the wrongly ingested data from scheduler of GMR
        if nodalId == 232:
            merged_df=merged_df[(merged_df['date']<'2021-06-18')|(merged_df['date']>'2021-09-24')]
        ''' Split the data into train and test '''
        train_data, test_data = divide_train_test(FxDate,merged_df)
        ''' Get the input and output variables in train and test data '''
        X_train, y_train, X_test, y_test = split_X_Y(train_data, test_data)
    
    elif saved == 1:
        start = timer()
        avc = apis.get_avc(nodalId, orgId, pssId, FxDate, FxDate, headers,projType = "SOLAR")
        end = timer()
        print("Time elapsed in getting avc is {}".format(end-start))
        start = timer()
        solcast = data_fe(nodalId, orgId, pssId, FxDate, FxDate)
        end = timer()
        print("Time elapsed in getting Solcast data is {}".format(end-start))
        solcast = solcast[['block_id', 'Datatime', 'temp', 'dhi', 'dni', 'refl_rad', 'sun_height', 'date']]
        X_train = pd.DataFrame()
        y_train = pd.DataFrame()
        X_test = pd.DataFrame()
        y_test = pd.DataFrame()
    input = solcast[solcast['date']==FxDate]
    input = input.reset_index()
    #input = input[['temp', 'dhi','dni','ws', 'sun_height', 'refl_rad']]
    input = input[['temp', 'dhi','dni', 'sun_height', 'refl_rad']]
    input = input.astype('float')
    
    return(X_train, y_train, X_test, y_test, input, avc)

def divide_train_test(FxDate,df):
    print("type of FxDate is ", (FxDate))
    # create a date column in data
    df['Date'] = df["Datatime"].dt.strftime('%Y-%m-%d')
    df["Date"] = pd.to_datetime(df["Date"])
    # choose FxDate data as test and rest as train
    print("type of FxDate is ", type(FxDate))
    print("type of df.Date is ", type(df["Date"]))
    test_data = df[(df['Date'] == FxDate)]
    train_data = df[(df['Date'] < FxDate)]
    print("FxDate in divide_train_test function is ", (FxDate))
    print("tail of train_data in  divide_train_test function is ", train_data.tail())


    # return data for data processing
    return train_data, test_data

def split_X_Y(train_data, test_data):
    #train_data = train_data[['Power', 'temp', 'dhi','dni', 'ws','sun_height', 'refl_rad']]
    #test_data = test_data[['Power', 'temp', 'dhi','dni', 'ws','sun_height', 'refl_rad']]
    train_data = train_data[['Power', 'temp', 'dhi','dni', 'sun_height', 'refl_rad']]
    test_data = test_data[['Power', 'temp', 'dhi','dni', 'sun_height', 'refl_rad']]
    # split into input (X) and output (Y) variables
    #X_train = train_data[['temp', 'dhi','dni', 'ws','sun_height', 'refl_rad']]
    X_train = train_data[['temp', 'dhi','dni','sun_height', 'refl_rad']]
    y_train =  train_data[['Power']]
    X_train = X_train.astype('float')
    y_train = y_train.astype('float')
    
    #X_test = test_data[['temp', 'dhi','dni', 'ws','sun_height', 'refl_rad']]
    X_test = test_data[['temp', 'dhi','dni', 'sun_height', 'refl_rad']]
    y_test =  test_data[['Power']]
    X_test = X_test.astype('float')
    y_test = y_test.astype('float')
    return X_train, y_train, X_test, y_test

def save_trained_model_path(nodalId, orgId, pssId, StartDate, FxDate):
    datetime64Obj = np.datetime64(FxDate) 
    save_path = f"C:/KREATE/PYTHON_TASKS/arjun/OPEN_DATA/SAVED_MODEL"+"/"+str(orgId)+"/"+str(pssId)+"/"+str(nodalId)
    save_path = save_path + "/"+str(datetime64Obj.astype(object).year)+"/"+str(datetime64Obj.astype(object).month)+"/"+str(datetime64Obj.astype(object).day)
    save_model = save_path+"/"+str(FxDate)+"_"+str(orgId)+"_"+str(pssId)+"_"+str(nodalId)+"_saved_model"
    # Save the entire model as a saved_model.
    #!mkdir -p save_model
    
    
    # determine the number of input features
    #path = f'/home/arjun/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/TRAINED_MODEL_DAYWISE'
    #path = f'/home/arjun/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/TRAINED_MODEL_DAYWISE_LIVE_DATA'
    #path = f'/home/arjun/KREATE/PYTHON_TASKS/OPEN_DATA/OUTPUT/TRAINED_MODEL_DAYWISE_WITHOUT_WS'
    #Path(path).mkdir(parents=True, exist_ok=True)
    #model_save_time = path+"/"+FxDate+".yaml"
    return(save_model)

def train_model_save(X_train,y_train,model_save_time):
    # determine the number of input features
    n_features = X_train.shape[1]
    
    # define deeper model based on paper
    def deeper_model():
        # create model
        model = Sequential()
        model.add(Dense(n_features, input_dim= n_features, kernel_initializer='normal', activation='relu'))
        model.add(Dense(n_features + 1, kernel_initializer='normal', activation='relu'))
        model.add(Dense((2/3 * n_features), kernel_initializer='normal', activation='relu'))
        model.add(Dense(1, kernel_initializer='normal'))
        # Compile model
        model.compile(loss="mse", optimizer='adam', metrics=[tf.keras.metrics.MeanSquaredError()])
        return model
    
    #estimator = KerasRegressor(build_fn=deeper_model)
    estimator = deeper_model()
    # fit the model
    start = timer()
    estimator.fit(X_train, y_train, epochs=20, batch_size=5, verbose= 2)
    end = timer()
    print("Time elapsed in fitting the models is {}".format(end-start))

    #get the summary of the model
    '''
    estimator.model.summary()
    #get the weights of the models
    weights = estimator.model.layers[0].get_weights()[0]
    biases = estimator.model.layers[0].get_weights()[1]
    '''
    estimator.summary()
    #get the weights of the models
    weights = estimator.layers[0].get_weights()[0]
    biases = estimator.layers[0].get_weights()[1]
    print("weights",weights  )
    print("length of weights",  len(weights))
    print("biases",biases  )
    print("length of biases",  len(biases))
    # Save the entire model as a SavedModel.
    estimator.save(model_save_time) 
    
    '''
    #Serialize model to yaml
    
    model_yaml = estimator.model.to_yaml()
    with open(model_save_time, "w") as yaml_file:
        yaml_file.write(model_yaml)
    # serialize weights to HDF5
    estimator.model.save_weights("model.h5")
    '''
    print("Saved model to disk")
    return(estimator)

def post_process(FxDate,pred,input,avc,sf_capacity,fxtype,blkid):
    flat_pred = [item for sublist in pred for item in sublist]
    data = {"Predicted_power": flat_pred}
    output = pd.DataFrame(data )
    output["block_id"] = range(1,97)
    output["Predicted_power"] = np.where( (output["block_id"] >20) & (output["block_id"] < 78), output["Predicted_power"], 0)
    output["Predicted_power"] = np.where( (output["Predicted_power"] <0) , 0 , output["Predicted_power"])
    output = output.merge(avc, how='inner', on=['block_id'])
    output['Predicted_power'] = output['Predicted_power']*(output['ACT_AVL_QTM']/sf_capacity)  
    start = FxDate + " " + "00:00:00"
    end = FxDate + " " + "23:45:00"
    output["Datetime"] = pd.date_range(start,end,freq="15T")
    output = pd.concat([output.reset_index(drop=True), input[["dni","temp"]].reset_index(drop=True)], axis=1)
    output = output[['nodalId', 'Datetime', 'block_id', 'Predicted_power', 'dni', 'temp']]
    if int(fxtype)==1:
        output = output[output['block_id']>= int(blkid)]
        print("Fx inside loop is intraday with FxBlockId : ", blkid)
    return(output)
    
def plot_output(FxDate,output):
    print("FxDate in plot is :",FxDate)
    import matplotlib.pyplot as plt
    FxDate = FxDate
    f = plt.figure()
    f.set_figwidth(14)
    f.set_figheight(7)
    plt.plot(output.block_id,output.Predicted_power,label = "Predicted_power")
    plt.xlabel('block_id')
    # Set the y axis label of the current axis.
    plt.ylabel('Power')
    # Set a title of the current axes.
    plt.title(f"Predicted Power Plot of {FxDate}" )
    plt.legend()
    plt.show()
